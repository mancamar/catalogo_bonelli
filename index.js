/* eslint-disable  func-names */
/* eslint-disable  no-console */

//const Alexa = require('ask-sdk');
const Alexa = require('ask-sdk-core');
var https = require('https');
//const skillBuilder = Alexa.SkillBuilders.standard();
const skillBuilder = Alexa.SkillBuilders.custom();
const { DynamoDbPersistenceAdapter } = require('ask-sdk-dynamodb-persistence-adapter');

const maxResult = 4;
const maxStoreResultInSession = 60;

const globalRepromptText = 'Prova a dire edicola per conoscere i fumetti in edicola, ' + getBreak("short") +
  'prossimamente per le prossime uscite, ' + getBreak("short") +
  'arretrati per trovare gli arretrati, ' + getBreak("short") +
  'cerca autore per i fumetti scritti o disegnati da un autore, ' + getBreak("short") +
  'prova a dire conta per sapere quanti fumetti ha pubblicato un autore,' + getBreak("short") +
  'aiuto per ricevere aiuto' + getBreak("short") +
  "prova a dire esempio per ricevere un suggerimento " + getBreak("short") + " cosa vuoi fare ora?";
const hint = [
  "cerca quanti fumetti ha scritto paola barbato nel 2015",
  "dimmi quanti numeri ha disegnato gigi cavenago nel 2018",
  "cerca quanti dylan dog ha scritto paola barbato",
  "cerca quanti numeri di tex ha scritto galeppini",
  "cerca quanti dylan dog ha scritto paola barbato nel 2019",
  "cerca quanti numeri di dylan dog ha scritto paola barbato nel 2019",
  "dimmi i fumetti in edicola",
  "cerca i fumetti in edicola",
  "cerca i fumetti di recchioni in edicola",
  "cerca i fumetti di recchioni ora in edicola",
  "cerca i fumetti di recchioni adesso in edicola",
  "cerca i fumetti scritti da recchioni in edicola",
  "cerca i fumetti scritti da recchioni ora in edicola",
  "cerca i fumetti scritti da recchioni adesso in edicola",
  "cerca i dylan dog in edicola",
  "dimmi i dylan dog in edicola",
  "cerca i fumetti in uscita questa settimana",
  "dimmi i fumetti in uscita questo mese",
  "cerca i fumetti di dylan dog scritti da recchioni in edicola",
  "cerca i fumetti prossimamente in edicola",
  "dimmi i fumetti in uscita",
  "dimmi i fumetti prossimamente in uscita",
  "dimmi i fumetti prossimamente in edicola",
  "cerca i fumetti di cavenago in uscita prossimamente",
  "cerca i fumetti disegnati da cavenago in uscita prossimamente",
  "cerca i fumetti scritti da recchioni in uscita prossimamente",
  "dimmi i fumetti prossimamente in edicola di recchioni",
  "dimmi cosa esce prossimamente in edicola di recchioni",
  "dimmi quando esce il prossimo dylan dog",
  "dimmi quando esce il prossimo numero di dylan dog",
  "dimmi quando esce il nuovo numero di dylan dog",
  "dimmi quando esce il nuovo dylan dog",
  "trova il prossimo dylan dog",
  "trova il nuovo dylan dog",
  "cerca i dylan dog di recchioni in uscita prossimamente",
  "cerca i dylan dog di recchioni prossimamente in uscita",
  "cerca i dylan dog di recchioni prossimamente in edicola",
  "cerca i dylan dog di recchioni in edicola prossimamente",
  "cerca i numeri di dylan dog scritti da recchioni prossimamente in edicola",
  "cerca i numeri di dylan dog scritti da recchioni in edicola prossimamente",
  "cerca i numeri di dylan dog di recchioni prossimamente in edicola",
  "cerca i numeri di dylan dog di recchioni in edicola prossimamente",
  "cerca i dylan dog del 2020",
  "cerca i dylan dog pubblicati nel 2020",
  "cerca i dylan dog usciti nel 2020",
  "cerca i fumetti di Recchioni pubblicati nel 2020",
  "cerca i fumetti che ha scritto Recchioni nel 2020",
  "cerca i fumetti che ha scritto la Barbato nel 2020",
  "cerca quali fumetti ha scritto la Barbato nel 2020",
  "cerca quali fumetti ha disegnato Cavenago nel 2020",
  "cerca dylan dog 100",
  "cerca dylan dog numero 100",
  "cerca il numero 363 di dylan dog",
  "cerca informazioni sul numero 363 di dylan dog",
  "dimmi quello che sai su dylan dog numero 400",
  "dimmi quello che sai su dylan dog 400",
  "dimmi informazioni su dylan dog numero 400",
  "dimmi informazioni su dylan dog 400",
  "cerca il dylan dog numero 100",
  "cerca il dylan dog 100",
  "cerca i fumetti scritti da paola barbato",
  "cerca i fumetti che ha disegnato gigi cavenago",
  "cerca i fumetti che ha scritto la barbato",
  "dimmi quali fumetti ha scritto la barbato",
  "dimmi quali fumetti ha disegnato cavenago",
  "cerca i dylan dog disegnati da gigi cavenago",
  "cerca i dylan dog disegnati da daniele caluri",
  "dimmi quali dylan dog ha disegnato daniele caluri",
  "dimmi quali numeri di dylan dog ha disegnato caluri",
  "dimmi quali numeri di dylan dog ha scritto la barbato",
  "dimmi i dylan dog scritti da recchioni",
  "cerca quali dylan dog ha scritto la barbato nel 2019",
  "cerca quali dylan dog ha disegnato cavenago nel 2019",
  "cerca quali numeri di dylan dog ha disegnato cavenago nel 2019",
  "cerca quali numeri di dylan dog ha scritto la barbato nel 2019",
  "cerca i dylan dog disegnati da gigi cavenago nel 2019",
  "cerca i dylan dog scritti da paola barbato nel 2020",
  "cerca l'ultimo dylan dog",
  "trova l'ultimo numero di dylan dog",
  "cerca l'ultimo dylan dog scritto da paola barbato",
  "trova l'ultimo dylan dog disegnato da gigi cavenago",
  "trova l'ultimo fumetto di paola barbato",
  "cerca l'ultimo fumetto disegnato da gigi cavenago",
  "dimmi quale è il primo fumetto pubblicato da Nicola Mari",
  "dimmi quale è il primo dylan dog disegnato da Gigi Simeoni",
  "dimmi quanti sono i fumetti scritti da gigi cavenago nel 2019"
];

function storePersistentAttributes(handlerInput) {
  return new Promise((resolve, reject) => {
    handlerInput.attributesManager.savePersistentAttributes()
      .then(() => {
        resolve();
      })
      .catch((err) => {
        reject(err);
      });
  });
}

function supportsAPL(handlerInput) {
  const supportedInterfaces = handlerInput.requestEnvelope.context.System.device.supportedInterfaces;
  const aplInterface = supportedInterfaces['Alexa.Presentation.APL'];
  return aplInterface != null && aplInterface != undefined;
}

function getHint() {
  return hint[Math.floor(Math.random() * hint.length)];
}

function httpGet(requestPath, callback) {
  let myAPI = {
    host: 'tare.isti.cnr.it',
    port: 443,
    path: requestPath,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  };
  myAPI.path = encodeURI(myAPI.path);
  const req = https.request(myAPI, (res) => {
    res.setEncoding('utf8');
    let returnData = '';

    res.on('data', (chunk) => {
      returnData += chunk;
    });
    res.on('end', () => {
      if (res.statusCode === 200) {
        try {
          const retObj = JSON.parse(returnData)
          callback(retObj);
        } catch(err) {
          callback(undefined);
        }
      } else {
        callback(undefined);
      }
    });
  });

  req.on('error', (error) => {
    console.log("error", error);
    callback(undefined)
  })
  req.end()
}

function getItalianRole(role, type) {
  switch (role) {
    case 'subject':
    case 'script':
      if (type === 'single') {
        return "scritto"
      } else {
        return "scritti";
      }
    case 'illustration':
    case 'cover':
      if (type === 'single') {
        return "disegnato";
      } else {
        return "disegnati";
      }
    default:
      if (type === 'single') {
        return "pubblicato";
      } else {
        return "pubblicati";
      }
  }
}

function getItalianDate(date) {
  var dateSplit = date.split("-");
  return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
}

function getSlotValue(slotName, intentObj) {
  if (intentObj !== undefined && intentObj.slots !== undefined && intentObj.slots[slotName] !== undefined) {
    if (slotName === 'comic_name') {
      var comicName = "";
      if (intentObj !== undefined && intentObj.slots !== undefined && intentObj.slots["comic_name"] !== undefined) {
        if (intentObj.slots["comic_name"].resolutions !== undefined &&
          intentObj.slots["comic_name"].resolutions.resolutionsPerAuthority !== undefined &&
          intentObj.slots["comic_name"].resolutions.resolutionsPerAuthority[0] !== undefined &&
          intentObj.slots["comic_name"].resolutions.resolutionsPerAuthority[0].status !== undefined &&
          intentObj.slots["comic_name"].resolutions.resolutionsPerAuthority[0].status.code === 'ER_SUCCESS_MATCH' &&
          intentObj.slots["comic_name"].resolutions.resolutionsPerAuthority[0].values !== undefined &&
          intentObj.slots["comic_name"].resolutions.resolutionsPerAuthority[0].values.length > 0) {
          comicName = intentObj.slots["comic_name"].resolutions.resolutionsPerAuthority[0].values[0].value.id;
          if (comicName == '') {
            comicName = intentObj.slots["comic_name"].value;
          }
        } else {
          comicName = intentObj.slots["comic_name"].value;
        }
      }
      if (comicName.includes("_")) {
        comicName = comicName.split("_").join(" ");
      }
      return comicName;
    } else {
      return intentObj.slots[slotName].value;
    }
  } else {
    return undefined;
  }
}

function getAuthor(intentObj) {
  //type nameSurname: se c'è trattino - si tratta di nameSurname. es. Luigi-Piccatto rimuovo trattino e lo sostiuisco con lo spazio
  //type surname:     se c'è underscore _ si tratta di spazio (surname) es: De_Tommaso rimuovo underscore e lo sostiuisco con lo spazio
  //type surname:     se non c'è nessuno dei due si tratta si surname
  let author = undefined;
  if (intentObj !== undefined && intentObj.slots !== undefined && intentObj.slots["author_name"] !== undefined) {
    if (intentObj.slots["author_name"].resolutions !== undefined &&
      intentObj.slots["author_name"].resolutions.resolutionsPerAuthority !== undefined &&
      intentObj.slots["author_name"].resolutions.resolutionsPerAuthority[0] !== undefined &&
      intentObj.slots["author_name"].resolutions.resolutionsPerAuthority[0].status !== undefined &&
      intentObj.slots["author_name"].resolutions.resolutionsPerAuthority[0].status.code === 'ER_SUCCESS_MATCH' &&
      intentObj.slots["author_name"].resolutions.resolutionsPerAuthority[0].values !== undefined &&
      intentObj.slots["author_name"].resolutions.resolutionsPerAuthority[0].values.length > 0) {
      author = intentObj.slots["author_name"].resolutions.resolutionsPerAuthority[0].values[0].value.name;
      if (author == '') {
        author = intentObj.slots["author_name"].value;
      }
    } else {
      author = intentObj.slots["author_name"].value;
    }
  }
  if (author !== undefined && author !== '') {
    if (author.includes("-")) {
      author = author.split("-").join(" ");
      author = author.split("_").join(" ");
      return { id: author, type: 'authorNameSurname' };
    } else if (author.includes("_")) {      
      author = author.split("_").join(" ");
      return { id: author, type: 'authorSurname' };
    } else {
      return { id: author, type: 'authorSurname' };
    }
  } else {
    return undefined;
  }
}

function areSameSubjectAndScript(authorSubject, authorScript) {
  if (authorSubject.length === authorScript.length) {
    for (var i = 0; i < authorSubject.length; i++) {
      var found = false;
      subjectLabel: for (var j = 0; j < authorScript.length; j++) {
        if (authorSubject[i] === authorScript[j]) {
          found = true;
        }
        if (found) {
          break subjectLabel;
        }
      }
      if (!found) {
        return false;
      }
    }
  } else {
    return false;
  }
  return true;
}

function getAuthorsFromRole(authorsArray) {
  var toRet = "";
  if (authorsArray.length === 1) {
    return authorsArray[0];
  } else if (authorsArray.length === 2) {
    toRet += authorsArray[0] + ", ";
    toRet += authorsArray[1];
  } else {
    for (var i = 0; i < authorsArray.length - 1; i++) {
      toRet += authorsArray[i] + ", ";
    }
    toRet += authorsArray[authorsArray.length - 1];
  }
  return toRet;
}

function getProsody(text) {
  return "<prosody pitch='medium' rate='medium'>" + text + "</prosody>";
}

function getBreak(type) {
  switch (type) {
    case 'short':
      return "<break time='250ms'/>";
    case 'medium':
      return "<break time='400ms'/>";
    case 'long':
      return "<break time='800ms'/>";
    default:
      return '';
  }
}

function sayAsDate(date) {
  return "<say-as interpret-as='date'>" + date + "</say-as>";
}

function sayAsOrdinal(number) {
  return "<say-as interpret-as='ordinal'>" + number + "</say-as>";
}

function cleanSummary(summary, authors) {
  summary = summary.toLowerCase();
  if (summary.startsWith("soggetto")) {
    var idxLabelCover = summary.indexOf("copertina:");
    var coverAuthor = authors.cover[0].toLowerCase();
    if (idxLabelCover !== -1) {
      var idxCoverAuthor = summary.indexOf(coverAuthor, idxLabelCover);
      if (idxCoverAuthor !== -1) {
        var newSummary = summary.substring(idxCoverAuthor + coverAuthor.length, summary.length);
        summary = newSummary;
      }
    } else {
      var idxLabelCover = summary.indexOf("colori:");
      if (idxLabelCover !== -1) {
        var newSummary = summary.substring(idxLabelCover + 14, summary.length);
        summary = newSummary;
      }
    }
  }
  return summary;
}

function getAuthorInfo(res) {
  let authors = {
    subject: [],
    script: [],
    illustration: [],
    cover: [],
    color: [],
    cover_color: [],
    unknown: []
  }
  let speechOutput = "";
  let cardAuthorInfo = "";
  for (var i = 0; i < res.pubblicationAuthors.length; i++) {
    authors[res.pubblicationAuthors[i].pk.role].push(res.pubblicationAuthors[i].pk.author.nameSurname)
  }
  
  if (areSameSubjectAndScript(authors.subject, authors.script)) {
    speechOutput += " soggetto e sceneggiatura di " + getProsody(getAuthorsFromRole(authors.subject)) + getBreak('short');
    cardAuthorInfo += "Soggetto e sceneggiatura di " + getAuthorsFromRole(authors.subject) + "<br>";
  } else {
    speechOutput += " soggetto di " + getProsody(getAuthorsFromRole(authors.subject)) + getBreak('short');
    cardAuthorInfo += "Soggetto di " + getAuthorsFromRole(authors.subject) + "<br>";
    speechOutput += " sceneggiatura di " + getProsody(getAuthorsFromRole(authors.script)) + getBreak('short');
    cardAuthorInfo += " Sceneggiatura di " + getAuthorsFromRole(authors.script) + "<br>";
  }
  speechOutput += " disegni di " + getProsody(getAuthorsFromRole(authors.illustration)) + getBreak('short');
  cardAuthorInfo += "Disegni di " + getAuthorsFromRole(authors.illustration) + "<br>";
  speechOutput += " copertina di " + getProsody(getAuthorsFromRole(authors.cover)) + getBreak('short');
  cardAuthorInfo += "Copertina di " + getAuthorsFromRole(authors.cover) + "<br>";
  //speechOutput += " uscito il " + sayAsDate(res.pubblicationDate) + getBreak('short');
  //cardAuthorInfo += " uscito il " + getItalianDate(res.pubblicationDate) + "<br>";
  return {
    speechOutput: speechOutput,
    cardAuthorInfo: cardAuthorInfo,
    authors: authors
  };
}

function createDataSourceList(res, title) {
  var items = [];
  var max = ((res.length <= maxResult) ? res.length : maxResult);
  for (var i = 0; i < max; i++) {
    var tmp = {
      "listItemIdentifier": res[i].pubblicationName + "_" + res[i].number,
      "ordinalNumber": i + 1,
      "textContent": {
        "primaryText": {
          "type": "PlainText",
          "text": res[i].pubblicationName + " - " + res[i].number,
        },
        "secondaryText": {
          "type": "PlainText",
          "text": res[i].pubblicationTitle
        },
        "tertiaryText": {
          "type": "PlainText",
          "text": getItalianDate(res[i].pubblicationDate)
        }
      },
      "image": {
        "contentDescription": null,
        "smallSourceUrl": null,
        "largeSourceUrl": null,
        "sources": [
          {
            "url": res[i].smallCoverURL,
            "size": "small",
            "widthPixels": 0,
            "heightPixels": 0
          },
          {
            "url": res[i].smallCoverURL,
            "size": "large",
            "widthPixels": 0,
            "heightPixels": 0
          }
        ]
      },
      "token": res[i].pubblicationName + "_" + res[i].number
    };
    items.push(tmp);
  }
  return {
    "listTemplate1Metadata": {
      "type": "object",
      "objectId": "lt1Metadata",
      "backgroundImage": {
        "contentDescription": null,
        "smallSourceUrl": null,
        "largeSourceUrl": null,
        "sources": [
          {
            "url": "https://giove.isti.cnr.it/users/manca/background-opacity1.png",
            "size": "small",
            "widthPixels": 0,
            "heightPixels": 0
          },
          {
            "url": "https://giove.isti.cnr.it/users/manca/background-opacity1.png",
            "size": "large",
            "widthPixels": 0,
            "heightPixels": 0
          }
        ]
      },
      "title": title,
      "logoUrl": "https://giove.isti.cnr.it/users/manca/icon_108.png"
    },
    "listTemplate1ListData": {
      "type": "list",
      "listId": "lt1Sample",
      "totalNumberOfItems": items.length,
      "listPage": {
        "listItems": items
      }
    }
  };
}

function createSingleDataSource(res, cardAuthorInfo, title) {
  return {
    "bodyTemplate2Data": {
      "type": "object",
      "objectId": "bt2Sample",
      "backgroundImage": {
        "contentDescription": null,
        "smallSourceUrl": null,
        "largeSourceUrl": null,
        "sources": [
          {
            "url": "https://giove.isti.cnr.it/users/manca/background-opacity.png",
            "size": "small",
            "widthPixels": 1024,
            "heightPixels": 600
          },
          {
            "url": "https://giove.isti.cnr.it/users/manca/background-opacity.png",
            "size": "large",
            "widthPixels": 1024,
            "heightPixels": 600
          }
        ]
      },
      "title": title,
      "image": {
        "contentDescription": null,
        "smallSourceUrl": null,
        "largeSourceUrl": null,
        "sources": [
          {
            "url": res.smallCoverURL,
            "size": "small",
            "widthPixels": 0,
            "heightPixels": 0
          },
          {
            "url": res.bigCoverURL,
            "size": "large",
            "widthPixels": 0,
            "heightPixels": 0
          }
        ]
      },
      "textContent": {
        "title": {
          "type": "PlainText",
          "text": res.pubblicationName + " - " + res.number
        },
        "subtitle": {
          "type": "PlainText",
          "text": res.pubblicationTitle
        },
        "subtitle2": {
          "type": "PlainText",
          "text": "Data uscita " + getItalianDate(res.pubblicationDate)
        },
        "subtitle3": {
          "type": "PlainText",
          "text": cardAuthorInfo
        },
        "primaryText": {
          "type": "PlainText",
          "text": res.summary
        }
      },
      "logoUrl": "https://giove.isti.cnr.it/users/manca/icon_108.png",
      "hintText": "Prova a dire: \"Alexa, apri catalogo fumetti e " + getHint() + "\""
    }
  };
}

function getAuthorRole(res, authorId) {
  for (var j = 0; j < res.pubblicationAuthors.length; j++) {
    if (res.pubblicationAuthors[j].pk.author.nameSurname === authorId ||
      res.pubblicationAuthors[j].pk.author.surname === authorId) {
      return res.pubblicationAuthors[j].pk.role;
    }
  }
  return undefined;
}

const LaunchRequestHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return request.type === 'LaunchRequest';
  },
  handle(handlerInput) {
    return new Promise((resolve, reject) => {
      const { attributesManager } = handlerInput;
      attributesManager.getPersistentAttributes()
        .then((persistentAttributes) => {
          persistentAttributes = persistentAttributes || {};
          if (!persistentAttributes['launchCount']) {
            persistentAttributes['launchCount'] = 0;
          }
          persistentAttributes['launchCount'] += 1;
          persistentAttributes['lastUseTimestamp'] = new Date(handlerInput.requestEnvelope.request.timestamp).getTime();
          handlerInput.attributesManager.setPersistentAttributes(persistentAttributes);
          storePersistentAttributes(handlerInput);
          if (persistentAttributes['launchCount'] === 1) {
            const speechText = 'Benvenuto nel catalogo fumetti,' +
              'sembra che questa sia la prima volta che usi questa skill' + getBreak("short") +
              'prova a dire edicola per trovare i fumetti Bonelli ora in edicola ' + getBreak("short") +
              'prossimamente per trovare le prossime uscite' + getBreak("short") +
              'arretrati per trovare gli arretrati' + getBreak("short") +
              'cerca autore per trovare i fumetti di un autore' + getBreak("short") +
              'prova a dire conta per sapere quanti fumetti ha pubblicato un autore' + getBreak("short") +
              'oppure prova a dire esempio per conoscere un nuovo comando' + getBreak("short") +
              'Potrai chiedere aiuto in ogni momento' + getBreak("short") +
              'Cosa vuoi fare?';
            resolve(handlerInput.responseBuilder
              .speak(speechText)
              .reprompt(globalRepromptText)
              .getResponse());
          } else if (persistentAttributes['launchCount'] > 1 && persistentAttributes['launchCount'] < 6) {
            const speechText = 'Bentornato nel catalogo fumetti, ' +
              'prova a dire edicola per trovare i fumetti Bonelli ora in edicola ' + getBreak("short") +
              'prossimamente per trovare le prossime uscite' + getBreak("short") +
              'arretrati per trovare gli arretrati' + getBreak("short") +
              'cerca autore per trovare i fumetti di un autore' + getBreak("short") +
              'prova a dire conta per sapere quanti fumetti ha pubblicato un autore' + getBreak("short") +
              'oppure prova a dire esempio per conoscere un nuovo comando' + getBreak("short") +
              'Potrai chiedere aiuto in ogni momento' + getBreak("short") +
              'Cosa vuoi fare?';
            resolve(handlerInput.responseBuilder
              .speak(speechText)
              .reprompt(globalRepromptText)
              .getResponse());
          } else {
            const speechText = 'Bentornato nel catalogo fumetti, prova a dire edicola ' +getBreak("short")+
              ' prossime uscite '+getBreak("short")+' arretrati '+getBreak("short")+' cerca autore oppure '+getBreak("short")+' prova a dire conta ' + getBreak("short") +
              'Cosa vuoi fare?';
            resolve(handlerInput.responseBuilder
              .speak(speechText)
              .reprompt(globalRepromptText)
              .getResponse());
          }
        })
    })
  },
}

const HelpIntentHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest' &&
      request.intent.name === 'AMAZON.HelpIntent')
  },
  handle(handlerInput) {
    return handlerInput.responseBuilder
      .speak(globalRepromptText)
      .reprompt("Prova a dire " + getBreak("short") + getHint())
      .getResponse();
  }
};

const GetExampleRequestHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'GetExample');
  },
  handle(handlerInput) {
    const speechText = "Prova a dire " + getHint() + getBreak("short") + " ora puoi riprovare a farmi una domanda ";
    const repromptText = "Prova a dire " + getHint() + getBreak("short") + " ora puoi riprovare a farmi una domanda ";
    return handlerInput.responseBuilder
      .speak(speechText)
      .reprompt(repromptText)
      .getResponse();
  },
}

const CountPubByAuthor = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'CountPubByAuthor');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;
    let author = getAuthor(intentObj);
    if(author === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo autore, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    sessionAttributes.authorType = author.type;
    sessionAttributes.authorId = author.id;
    sessionAttributes.authorName = getSlotValue("author_name", intentObj);
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/countPublications/" + author.type + "/" + author.id;
      httpGet(path, (res) => {
        let speechOutput = "";
        if (res === 0 || res === undefined) {
          speechOutput = "Non mi risulta che " + getSlotValue("author_name", intentObj) + " abbia pubblicato un fumetto Bonelli ";
          speechOutput += getBreak("short")+" Cosa vuoi fare ora?"
        } else if (res === 1) {
          speechOutput = getSlotValue("author_name", intentObj) + " ha pubblicato un solo fumetto"
          speechOutput += getBreak("short") + " vuoi sapere qual è?";
        } else {
          speechOutput = getSlotValue("author_name", intentObj) + " ha pubblicato " + res + " fumetti"
          speechOutput += getBreak("short") + " vuoi sapere quali sono?";
        }
        resolve(handlerInput.responseBuilder
          .speak(speechOutput)
          .reprompt(globalRepromptText)
          .getResponse());
      });
    });

  }
};

const CountPubByAuthorAndYear = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'CountPubByAuthorAndYear');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;
    let author = getAuthor(intentObj);
    let year = getSlotValue("year", intentObj);
    if(author === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo autore, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    if(year === undefined || isNaN(year)) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, sembra che tu non mi abbia detto l'anno in maniera corretta, prova a farmi un'altra domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    sessionAttributes.year = year;
    sessionAttributes.authorName = getSlotValue("author_name", intentObj);
    sessionAttributes.authorType = author.type;
    sessionAttributes.authorId = author.id;
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/countPublications/" + author.type + "/" + author.id + "/year/" + year;
      httpGet(path, (res) => {
        let speechOutput = "";
        if (res === 0 || res === undefined) {
          speechOutput = "Non mi risulta che " + getSlotValue("author_name", intentObj) + " abbia pubblicato un fumetto nel " + year + " " + getBreak("short");
          speechOutput += " Cosa vuoi fare ora?"
        } else if (res === 1) {
          speechOutput = getSlotValue("author_name", intentObj) + " ha pubblicato un solo fumetto durante l'anno "+year
          speechOutput += getBreak("short") + " vuoi sapere qual è?";
        } else {
          speechOutput = getSlotValue("author_name", intentObj) + " ha pubblicato " + res + " fumetti durante l'anno " + year;
          speechOutput += getBreak("short") + " vuoi sapere quali sono?";
        }
        resolve(handlerInput.responseBuilder
          .speak(speechOutput)
          .reprompt(globalRepromptText)
          .getResponse());
      });
    });

  }
};

const CountPubByPubNameAndAuthor = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'CountPubByPubNameAndAuthor');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;
    let author = getAuthor(intentObj);
    let pubName = getSlotValue("comic_name", intentObj);
    if(author === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo autore, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    if(pubName === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo fumetto, conosco tanti fumetti, ad esempio Dylan Dog e Tex, prova a farmi un'altra domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    sessionAttributes.pubName = pubName;
    sessionAttributes.authorName = getSlotValue("author_name", intentObj);
    sessionAttributes.authorType = author.type;
    sessionAttributes.authorId = author.id;
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/countPublications/pubblicationName/" + pubName + "/" + author.type + "/" + author.id;
      httpGet(path, (res) => {
        let speechOutput = "";
        if (res === 0 || res === undefined) {
          speechOutput = "Non mi risulta che " + getSlotValue("author_name", intentObj) + " abbia pubblicato un numero di " + pubName;
          speechOutput += getBreak("short")+" Cosa vuoi fare ora?"
        } else if (res === 1) {
          speechOutput = getSlotValue("author_name", intentObj) + " ha pubblicato un solo numero di "+pubName
          speechOutput += getBreak("short") + " vuoi sapere qual è?";
        } else {
          speechOutput = getSlotValue("author_name", intentObj) + " ha pubblicato " + res + " numeri di " + pubName
          speechOutput += getBreak("short") + " vuoi sapere quali sono?";
        }
        resolve(handlerInput.responseBuilder
          .speak(speechOutput)
          .reprompt(globalRepromptText)
          .getResponse());
      });
    });

  }
};

const CountPubByPubNameAndAuthorAndYear = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'CountPubByPubNameAndAuthorAndYear');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;
    let author = getAuthor(intentObj);
    let pubName = getSlotValue("comic_name", intentObj);
    let year = getSlotValue("year", intentObj);
    if(author === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo autore, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
	  if(year === undefined || isNaN(year)) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, sembra che tu non mi abbia detto l'anno in maniera corretta, prova a farmi un'altra domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
	  if(pubName === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo fumetto, conosco tanti fumetti, ad esempio Dylan Dog e Tex, prova a farmi un'altra domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    sessionAttributes.pubName = pubName;
    sessionAttributes.year = year;
    sessionAttributes.authorName = getSlotValue("author_name", intentObj);
    sessionAttributes.authorType = author.type;
    sessionAttributes.authorId = author.id;
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/countPublications/pubblicationName/" + pubName + "/" + author.type + "/" + author.id + "/year/" + year;
      httpGet(path, (res) => {
        let speechOutput = "";
        if (res === 0 || res === undefined) {
          speechOutput = "Non mi risulta che " + getSlotValue("author_name", intentObj) + " abbia pubblicato un numero di " + pubName + " durante l'anno " + year;
          speechOutput += getBreak("short")+" Cosa vuoi fare ora?"
        } else if (res === 1) {
          speechOutput = getSlotValue("author_name", intentObj) + " ha pubblicato un solo numero di "+pubName+" durante l'anno "+year;
          speechOutput += getBreak("short") + " vuoi sapere qual è?";
        } else {
          speechOutput = getSlotValue("author_name", intentObj) + " ha pubblicato " + res + " numeri di " + pubName + " durante l'anno " + year;
          speechOutput += getBreak("short") + " vuoi sapere quali sono?";
        }

        resolve(handlerInput.responseBuilder
          .speak(speechOutput)
          .reprompt(globalRepromptText)
          .getResponse());
      });
    });

  }
};

const FindPubInNewstand = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindPubInNewstand'));
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const request = handlerInput.requestEnvelope.request;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublicationInNewsstand";
      httpGet(path, (res) => {
        sessionAttributes.publications = [];
        if (res === undefined || res.length === 0) {
          resolve(handlerInput.responseBuilder
            .speak("Non ho trovato fumetti in edicola "+getBreak("short")+" cosa vuoi fare ora?")
            .reprompt(globalRepromptText)
            .getResponse());
        } else {
          let speechOutput = "Ecco i fumetti ora in edicola " + getBreak('short');
          let reprompt = "";
          for (var i = 0; i < res.length; i++) {
            if (i < maxResult) {
              speechOutput += res[i].pubblicationName + " numero " + res[i].number + getBreak('short') + " dal titolo " + getProsody(res[i].pubblicationTitle) + getBreak('medium');
            } else {
              sessionAttributes.publications.push({ pubblicationName: res[i].pubblicationName, number: res[i].number, pubblicationTitle: res[i].pubblicationTitle, pubblicationDate: res[i].pubblicationDate, smallCoverURL: res[i].smallCoverURL });
            }
          }
          if (res.length - maxResult > 0) {
            speechOutput += " Ci sono altri " + parseInt(res.length - maxResult) + " fumetti in elenco, vuoi che te li dica?" + getBreak('medium');
            reprompt = "Vuoi sentire gli altri " + parseInt(res.length - maxResult) + " fumetti in edicola?"
          }
          if (reprompt !== '') {
            if (supportsAPL(handlerInput)) {
              sessionAttributes.title = "Fumetti in edicola";
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt(reprompt)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/publication_list_template.json'),
                  datasources: createDataSourceList(res, "Fumetti in edicola")
                })
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt(reprompt)
                .getResponse());
            }
          } else {
            speechOutput += getBreak("short") + " cosa vuoi fare ora?";
            if (supportsAPL(handlerInput)) {
              sessionAttributes.title = "Fumetti in edicola";
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/publication_list_template.json'),
                  datasources: createDataSourceList(res, "Fumetti in edicola")
                })
                .reprompt(globalRepromptText)
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt(globalRepromptText)
                .getResponse());
            }
          }
        }
      });
    });

  }
};

const FindPubInNewstandByAuthor = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'FindPubInNewstandByAuthor');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    let author = getAuthor(intentObj);
    if(author === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo autore, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublicationInNewsstand/" + author.type + "/" + author.id;
      httpGet(path, (res) => {
        let speechOutput = "";
        let reprompt = "";
        let cardAuthorInfo = "";

        if (res !== undefined && res.length > 0) {
          if (res.length === 1) {
            const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
            sessionAttributes.previousIntent = handlerInput.requestEnvelope.request.intent.name;
            var role = getAuthorRole(res[0], author.id);
            let info = getAuthorInfo(res[0]);

            if (role !== undefined) {
              speechOutput = "Il " + res[0].pubblicationName + " " + getItalianRole(role, 'single') + " da " + getSlotValue("author_name", intentObj) + " ora in edicola è il numero " + res[0].number + getBreak('short');
            } else {
              speechOutput = "Il " + res[0].pubblicationName + " di " + getSlotValue("author_name", intentObj) + " ora in edicola è il numero " + res[0].number + getBreak('short');
            }
            speechOutput += " uscito il " + sayAsDate(res[0].pubblicationDate) + getBreak("short");
            speechOutput += info.speechOutput;
            cardAuthorInfo += info.cardAuthorInfo;
            speechOutput += " Vuoi sentire la sintesi della storia?" + getBreak('short');
            res[0].summary = cleanSummary(res[0].summary, info.authors)
            sessionAttributes.summary = res[0].summary;
            if (supportsAPL(handlerInput)) {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/single_publication.json'),
                  datasources: createSingleDataSource(res[0], cardAuthorInfo, "Ora in edicola")
                })
                .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
                .getResponse());
            }
          } else {
            var role = getAuthorRole(res[0], author.id);
            for (var i = 0; i < res.length; i++) {
              speechOutput += res[i].pubblicationName + " numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle) + getBreak('short') + " uscito il " + sayAsDate(res[i].pubblicationDate) + getBreak('short');
            }
            if (role !== undefined) {
              speechOutput = "Ecco i fumetti " + getItalianRole(role) + " da " + getSlotValue("author_name", intentObj) + " ora in edicola " + getBreak('short') + speechOutput;
            } else {
              speechOutput = "Ecco i fumetti di " + getSlotValue("author_name", intentObj) + " ora in edicola " + getBreak('short') + speechOutput;
            }
            speechOutput += getBreak("short") + " cosa vuoi fare ora?";
            if (supportsAPL(handlerInput)) {
              const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
              sessionAttributes.title = "Fumetti in edicola";
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/publication_list_template.json'),
                  datasources: createDataSourceList(res, "Fumetti in edicola")
                })
                .reprompt(globalRepromptText)
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt(globalRepromptText)
                .getResponse());
            }
          }
        } else {
          //no res
          speechOutput = "Non ho trovato fumetti di " + getProsody(getSlotValue("author_name", intentObj)) + " ora in edicola";
          speechOutput += getBreak("short") + " cosa vuoi fare ora?";
          resolve(handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt(globalRepromptText)
            .getResponse());
        }
      });
    });

  }
};

const FindPubInNewstandByPubName = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindPubInNewstandByPubName'));
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const intentName = handlerInput.requestEnvelope.request.intent.name;
    let pubName = getSlotValue("comic_name", intentObj);
    if(pubName === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo fumetto, ne conosco tanti altri, ad esempio Dylan Dog e Tex, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = handlerInput.requestEnvelope.request.intent.name;
    sessionAttributes.pubName = pubName;
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublicationInNewsstand/pubblicationName/" + pubName;
      httpGet(path, (res) => {
        if (res !== undefined && res.length > 0) {
          if (res.length === 1) {
            let info = getAuthorInfo(res[0]);
            let speechOutput = "Il " + pubName + " ora in edicola è il numero " + res[0].number + " dal titolo "
              + getProsody(res[0].pubblicationTitle) + getBreak("short");
            speechOutput += " uscito il " + sayAsDate(res[0].pubblicationDate) + getBreak("short");
            speechOutput += info.speechOutput;
            speechOutput += " Vuoi sentire la sintesi della storia?" + getBreak('short');
            res[0].summary = cleanSummary(res[0].summary, info.authors)
            sessionAttributes.summary = res[0].summary;

            let cardAuthorInfo = info.cardAuthorInfo;
            if (supportsAPL(handlerInput)) {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/single_publication.json'),
                  datasources: createSingleDataSource(res[0], cardAuthorInfo, "Ora in edicola")
                })
                .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
                .getResponse());
            }
          } else {
            let speechOutput = "Ecco i fumetti di " + pubName + " ora in edicola" + getBreak("short");
            for (var i = 0; i < res.length; i++) {
              speechOutput += "numero " + getProsody(res[i].number) + " dal titolo " + getBreak("short") + getProsody(res[i].pubblicationTitle)
                + getBreak('short') + " uscito il " + sayAsDate(res[i].pubblicationDate) + getBreak('short');
            }
            speechOutput += getBreak("short") + " cosa vuoi fare ora?";
            if (supportsAPL(handlerInput)) {
              sessionAttributes.title = "Fumetti in edicola";
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/publication_list_template.json'),
                  datasources: createDataSourceList(res, "Fumetti in edicola")
                })
                .reprompt(globalRepromptText)
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt(globalRepromptText)
                .getResponse());
            }
          }
        } else {
          resolve(handlerInput.responseBuilder
            .speak("Non ho trovato fumetti di " + getProsody(pubName) + " in edicola " + getBreak("short") + " cosa vuoi fare ora?")
            .reprompt(globalRepromptText)
            .getResponse());
        }
      });
    });

  }
};

const FindPubInNewstandByTimeInterval = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'FindPubInNewstandByTimeInterval');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;
    let timeInterval = getSlotValue("when_current", intentObj);
    if(timeInterval === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, puoi cercare i fumetti in edicola questa settimana o questo mese, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublicationInNewsstand/timeInterval/" + timeInterval;
      httpGet(path, (res) => {
        sessionAttributes.publications = [];
        var reprompt = "";
        let speechOutput = "";
        var speakInterval = "";
        sessionAttributes.speakInterval = speakInterval;

        if (res === undefined || res.length === 0) {
          if (timeInterval === 'settimana') {
            speechOutput = "Non ho trovato fumetti in uscita questa settimana";
          } else {
            speechOutput = "Non ho trovato fumetti in uscita questo mese";
          }
          speechOutput += getBreak("short") + " cosa vuoi fare ora?";
          resolve(handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt(globalRepromptText)
            .getResponse());
        } else {
          let comic = "i fumetti"
          if(res.length === 1) {
            comic = "il fumetto";
          }
          if (timeInterval === "mese") {
            speechOutput = "Ecco "+comic+" in edicola questo " + getProsody(timeInterval) + getBreak("short");
            speakInterval = "questo mese";
          } else {
            speechOutput = "Ecco "+comic+" in edicola questa " + getProsody(timeInterval) + getBreak("short");
            speakInterval = "questa settimana";
          }
          for (var i = 0; i < res.length; i++) {
            if (i < maxResult) {
              speechOutput += res[i].pubblicationName + " numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle) + getBreak("short");
              if (timeInterval === 'settimana') {
                speechOutput += " in uscita " + sayAsDate(res[i].pubblicationDate) + getBreak('short');
              }
            } else {
              sessionAttributes.publications.push({ pubblicationName: res[i].pubblicationName, number: res[i].number, pubblicationTitle: res[i].pubblicationTitle, pubblicationDate: res[i].pubblicationDate, smallCoverURL: res[i].smallCoverURL });
            }
          }
          if (res.length - maxResult > 0) {
            speechOutput += getBreak("short") + " Ci sono altri " + parseInt(res.length - maxResult) + " fumetti in uscita " + speakInterval + ", vuoi che te li dica?" + getBreak('medium');
            reprompt = "Vuoi sentire gli altri " + parseInt(res.length - maxResult) + " fumetti in uscita " + speakInterval + "?";
          }
          if (reprompt !== '') {
            if (supportsAPL(handlerInput)) {
              sessionAttributes.title = "Fumetti in uscita " + speakInterval;
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt(reprompt)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/publication_list_template.json'),
                  datasources: createDataSourceList(res, "Fumetti in uscita " + speakInterval)
                })
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt(reprompt)
                .getResponse());
            }
          } else {
            speechOutput += getBreak("short") + " cosa vuoi fare ora?";
            if (supportsAPL(handlerInput)) {
              sessionAttributes.title = "Fumetti in uscita " + speakInterval;
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/publication_list_template.json'),
                  datasources: createDataSourceList(res, "Fumetti in uscita " + speakInterval)
                })
                .reprompt(globalRepromptText)
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt(globalRepromptText)
                .getResponse());
            }
          }
        }
      });
    });
  }
};

const FindPubInNewstandByPubNameAndAuthor = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'FindPubInNewstandByPubNameAndAuthor');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    let pubName = getSlotValue("comic_name", intentObj);
    let author = getAuthor(intentObj);
    if(pubName === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo fumetto, ne conosco tanti altri, ad esempio Dylan Dog e Tex, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    if(author === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo autore, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublicationInNewsstand/pubblicationName/" + pubName + "/" + author.type + "/" + author.id;
      httpGet(path, (res) => {
        let speechOutput = "";
        if (res !== undefined && res.length > 0) {
          const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
          sessionAttributes.previousIntent = handlerInput.requestEnvelope.request.intent.name;

          var role = getAuthorRole(res[0], author.id);
          if (res.length === 1) {
            let info = getAuthorInfo(res[0]);
            if (role !== undefined) {
              speechOutput = "Il " + pubName + " " + getItalianRole(role, "single") + " da " + getSlotValue("author_name", intentObj) + " ora in edicola " + getBreak('short');
            } else {
              speechOutput = "Il " + pubName + " di " + getSlotValue("author_name", intentObj) + " ora in edicola " + getBreak('short');
            }
            speechOutput += " è il numero " + res[0].number + " dal titolo " + getProsody(res[0].pubblicationTitle) + getBreak('short');
            speechOutput += " uscito il " + sayAsDate(res[0].pubblicationDate) + getBreak('short')
            speechOutput += info.speechOutput;
            speechOutput += " Vuoi sentire la sintesi della storia?" + getBreak('short');
            res[0].summary = cleanSummary(res[0].summary, info.authors)
            sessionAttributes.summary = res[0].summary;
            let cardAuthorInfo = info.cardAuthorInfo;

            if (supportsAPL(handlerInput)) {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/single_publication.json'),
                  datasources: createSingleDataSource(res[0], cardAuthorInfo, "Ora in edicola")
                })
                .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
                .getResponse());
            }
          } else {
            for (var i = 0; i < res.length; i++) {
              speechOutput += " il numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle) + getBreak('short') + " uscito il " + sayAsDate(res[i].pubblicationDate) + getBreak('short');
            }
            if (role !== undefined) {
              speechOutput = "I numeri di " + pubName + " " + getItalianRole(role) + " da " + getProsody(getSlotValue("author_name", intentObj)) + " ora in edicola sono " + getBreak('short') + speechOutput;
            } else {
              speechOutput = "I numeri di " + pubName + " di " + getProsody(getSlotValue("author_name", intentObj)) + " ora in edicola sono " + getBreak('short') + speechOutput;
            }
            speechOutput += getBreak("short") + " cosa vuoi fare ora?";
            if (supportsAPL(handlerInput)) {
              sessionAttributes.title = "Fumetti in edicola";
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/publication_list_template.json'),
                  datasources: createDataSourceList(res, "Fumetti in edicola")
                })
                .reprompt(globalRepromptText)
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt(globalRepromptText)
                .getResponse());
            }
          }
        } else {
          resolve(handlerInput.responseBuilder
            .speak("Non ho trovato numeri di " + pubName + " pubblicati da " + getProsody(getSlotValue("author_name", intentObj)) + " ora in edicola" + getBreak("short") + " cosa vuoi fare ora?")
            .reprompt(globalRepromptText)
            .getResponse());
        }
      });
    });
  }
};

const FindNextPub = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindNextPub'));
  },
  handle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    const intentObj = handlerInput.requestEnvelope.request.intent;

    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findNextPublications";
      httpGet(path, (res) => {
        var reprompt = "";
        if (res !== undefined && res.length > 0) {
          const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
          sessionAttributes.previousIntent = handlerInput.requestEnvelope.request.intent.name;
          if (res.length === 1) {
            sessionAttributes.publications = [];
            let info = getAuthorInfo(res[0]);
            let speechOutput = "C'è un solo fumetto in uscita prossimamente " + getBreak('short')
            speechOutput += res[0].pubblicationName + " numero " + res[0].number + " dal titolo " + getProsody(res[0].pubblicationTitle) + getBreak('short');
            speechOutput += " uscito il " + sayAsDate(res[0].pubblicationDate) + getBreak('short');
            speechOutput += info.speechOutput;
            speechOutput += " Vuoi sentire la sintesi della storia?" + getBreak('short');
            res[0].summary = cleanSummary(res[0].summary, info.authors)
            sessionAttributes.summary = res[0].summary;
            let cardAuthorInfo = info.cardAuthorInfo;

            if (supportsAPL(handlerInput)) {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/single_publication.json'),
                  datasources: createSingleDataSource(res[0], cardAuthorInfo, "Prossimamente in edicola")
                })
                .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
                .getResponse());
            }
          } else {
            sessionAttributes.publications = [];
            let speechOutput = "Ecco i fumetti prossimamente in uscita " + getBreak("short");
            for (var i = 0; i < res.length; i++) {
              if (i < maxResult) {
                speechOutput += res[i].pubblicationName + " numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle) + getBreak("short");
              } else {
                sessionAttributes.publications.push({ pubblicationName: res[i].pubblicationName, number: res[i].number, pubblicationTitle: res[i].pubblicationTitle, pubblicationDate: res[i].pubblicationDate, smallCoverURL: res[i].smallCoverURL });
              }
            }

            if (res.length - maxResult > 0) {
              speechOutput += getBreak("short") + " Ci sono altri " + parseInt(res.length - maxResult) + " fumetti in uscita prossimamente, vuoi che te li dica?" + getBreak('medium');
              reprompt = "Vuoi sentire gli altri " + parseInt(res.length - maxResult) + " fumetti in uscita prossimamente?";
              if (supportsAPL(handlerInput)) {
                sessionAttributes.title = "Fumetti prossimamente in edicola";
                resolve(handlerInput.responseBuilder
                  .speak(speechOutput)
                  .reprompt(reprompt)
                  .addDirective({
                    type: 'Alexa.Presentation.APL.RenderDocument',
                    document: require('./apl/publication_list_template.json'),
                    datasources: createDataSourceList(res, "Fumetti prossimamente in edicola")
                  })
                  .getResponse());
              } else {
                resolve(handlerInput.responseBuilder
                  .speak(speechOutput)
                  .reprompt(reprompt)
                  .getResponse());
              }
            } else {
              speechOutput += getBreak("short") + " cosa vuoi fare ora?";
              if (supportsAPL(handlerInput)) {
                sessionAttributes.title = "Fumetti prossimamente in edicola";
                resolve(handlerInput.responseBuilder
                  .speak(speechOutput)
                  .reprompt(globalRepromptText)
                  .addDirective({
                    type: 'Alexa.Presentation.APL.RenderDocument',
                    document: require('./apl/publication_list_template.json'),
                    datasources: createDataSourceList(res, "Fumetti prossimamente in edicola")
                  })
                  .getResponse());
              } else {
                resolve(handlerInput.responseBuilder
                  .speak(speechOutput)
                  .reprompt(globalRepromptText)
                  .getResponse());
              }
            }
          }
        } else {
          resolve(handlerInput.responseBuilder
            .speak("Non ho trovato fumetti in prossimamente uscita " + getBreak("short") + " cosa vuoi fare ora?")
            .reprompt(globalRepromptText)
            .getResponse());
        }
      });
    });
  }
};

const FindNextPubByAuthor = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'FindNextPubByAuthor');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    let author = getAuthor(intentObj);
    if(author === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo autore, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findNextPublications/" + author.type + "/" + author.id;
      httpGet(path, (res) => {
        let speechOutput = "";
        if (res !== undefined && res.length > 0) {
          const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
          sessionAttributes.previousIntent = handlerInput.requestEnvelope.request.intent.name;
          var role = getAuthorRole(res[0], author.id);
          if (res.length === 1) {
            let info = getAuthorInfo(res[0]);
            if (role !== undefined) {
              speechOutput = "Il " + res[0].pubblicationName + " " + getItalianRole(role, "single") + " da " + getSlotValue("author_name", intentObj) + " prossimamente in edicola " + getBreak('short');
            } else {
              speechOutput = "Il " + res[0].pubblicationName + " di " + getSlotValue("author_name", intentObj) + " prossimamente in edicola " + getBreak('short');
            }
            speechOutput += " è il numero " + res[0].number + " dal titolo " + getProsody(res[0].pubblicationTitle) + getBreak('short');
            speechOutput += " in uscita il " + sayAsDate(res[0].pubblicationDate) + getBreak('short');
            speechOutput += info.speechOutput;
            speechOutput += " Vuoi sentire la sintesi della storia?" + getBreak('short');
            res[0].summary = cleanSummary(res[0].summary, info.authors)
            sessionAttributes.summary = res[0].summary;
            let cardAuthorInfo = info.cardAuthorInfo;

            if (supportsAPL(handlerInput)) {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/single_publication.json'),
                  datasources: createSingleDataSource(res[0], cardAuthorInfo, "Prossimamente in edicola")
                })
                .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
                .getResponse());
            }
          } else {
            for (var i = 0; i < res.length; i++) {
              speechOutput += res[i].pubblicationName + " numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle) + getBreak('short') + " uscito il " + sayAsDate(res[i].pubblicationDate) + getBreak('short');
            }
            if (role !== undefined) {
              speechOutput = "Ecco i fumetti " + getItalianRole(role) + " da " + getSlotValue("author_name", intentObj) + " prossimamente in edicola " + getBreak("short") + speechOutput;
            } else {
              speechOutput = "Ecco i fumetti di " + getSlotValue("author_name", intentObj) + " prossimamente in edicola " + getBreak("short") + speechOutput;
            }
            speechOutput += getBreak("short") + " cosa vuoi fare ora?";
            if (supportsAPL(handlerInput)) {
              sessionAttributes.title = "Fumetti prossimamente in edicola";
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/publication_list_template.json'),
                  datasources: createDataSourceList(res, "Fumetti prossimamente in edicola")
                })
                .reprompt(globalRepromptText)
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt(globalRepromptText)
                .getResponse());
            }
          }
        } else {
          speechOutput = "Non ho tovato fumetti pubblicati da " + getSlotValue("author_name", intentObj) + " prossimamente in uscita " + getBreak("short") + " cosa vuoi fare ora?";
          resolve(handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt(globalRepromptText)
            .getResponse());
        }
      });
    });
  }
};

const FindNextPubByPubName = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindNextPubByPubName'));
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const intentName = handlerInput.requestEnvelope.request.intent.name;
    let pubName = getSlotValue("comic_name", intentObj);
    if(pubName === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo fumetto, ne conosco tanti altri, ad esempio Dylan Dog e Tex, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findNextPublications/pubblicationName/" + pubName;
      httpGet(path, (res) => {
        if (res !== undefined && res.length > 0) {
          const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
          sessionAttributes.previousIntent = handlerInput.requestEnvelope.request.intent.name;

          if (res !== undefined && res.length === 1) {
            let info = getAuthorInfo(res[0]);
            let speechOutput = "Il numero di " + pubName + " prossimamente in uscita è il "
              + getProsody(res[0].number) + " dal titolo " + getBreak("short") + getProsody(res[0].pubblicationTitle) + getBreak('medium');
            speechOutput += " in uscita il " + getBreak('short') + sayAsDate(res[0].pubblicationDate);
            speechOutput += info.speechOutput;
            speechOutput += " Vuoi sentire la sintesi della storia?" + getBreak('short');
            res[0].summary = cleanSummary(res[0].summary, info.authors)
            sessionAttributes.summary = res[0].summary;
            let cardAuthorInfo = info.cardAuthorInfo;

            if (supportsAPL(handlerInput)) {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/single_publication.json'),
                  datasources: createSingleDataSource(res[0], cardAuthorInfo, "Prossimamente in edicola")
                })
                .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
                .getResponse());
            }
          } else {
            let speechOutput = "I numeri di " + pubName + " prossimamente in uscita sono ";
            for (var i = 0; i < res.length; i++) {
              speechOutput += "il numero " + getProsody(res[i].number) + " dal titolo " + getBreak("short") + getProsody(res[i].pubblicationTitle) + getBreak('medium') + " in uscita il " + getBreak('short') + sayAsDate(res[i].pubblicationDate);
            }
            speechOutput += getBreak("short") + " cosa vuoi fare ora?";
            if (supportsAPL(handlerInput)) {
              sessionAttributes.title = "Fumetti prossimamente in edicola";
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/publication_list_template.json'),
                  datasources: createDataSourceList(res, "Fumetti prossimamente in edicola")
                })
                .reprompt(globalRepromptText)
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt(globalRepromptText)
                .getResponse());
            }
          }
        } else {
          resolve(handlerInput.responseBuilder
            .speak("Non ho trovato numeri di " + pubName + " prossimamente in uscita " + getBreak("short") + " cosa vuoi fare ora?")
            .reprompt(globalRepromptText)
            .getResponse());
        }
      });
    });
  }
};

const FindNextPubByPubNameAndAuthor = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'FindNextPubByPubNameAndAuthor');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    let author = getAuthor(intentObj);
    let pubName = getSlotValue("comic_name", intentObj);
    if(author === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo autore, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    if(pubName === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo fumetto, ne conosco tanti altri, ad esempio Dylan Dog e Tex, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findNextPublications/pubblicationName/" + pubName + "/" + author.type + "/" + author.id;
      httpGet(path, (res) => {
        const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
        sessionAttributes.previousIntent = handlerInput.requestEnvelope.request.intent.name;
        let speechOutput = "";
        if (res !== undefined && res.length > 0) {
          var role = getAuthorRole(res[0], author.id);
          if (res.length === 1) {
            let info = getAuthorInfo(res[0]);
            if (role !== undefined) {
              speechOutput = "Il " + pubName + " " + getItalianRole(role, "single") + " da " + getSlotValue("author_name", intentObj) + " prossimamente in edicola " + getBreak('short');
            } else {
              speechOutput = "Il " + pubName + " di " + getSlotValue("author_name", intentObj) + " prossimamente in edicola " + getBreak('short');
            }
            speechOutput += " è il numero " + res[0].number + " dal titolo " + getProsody(res[0].pubblicationTitle) + getBreak('short');
            speechOutput += " uscito il " + sayAsDate(res[0].pubblicationDate) + getBreak('short');
            speechOutput += info.speechOutput;
            speechOutput += " Vuoi sentire la sintesi della storia?" + getBreak('short');
            res[0].summary = cleanSummary(res[0].summary, info.authors)
            sessionAttributes.summary = res[0].summary;
            let cardAuthorInfo = info.cardAuthorInfo;

            if (supportsAPL(handlerInput)) {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/single_publication.json'),
                  datasources: createSingleDataSource(res[0], cardAuthorInfo, "Prossimamente in edicola")
                })
                .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
                .getResponse());
            }
          } else {
            for (var i = 0; i < res.length; i++) {
              speechOutput += " il numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle) + getBreak('short') + " uscito il " + sayAsDate(res[i].pubblicationDate) + getBreak('short');
            }
            if (role !== undefined) {
              speechOutput = "I numeri di " + pubName + " " + getItalianRole(role) + " da " + getProsody(getSlotValue("author_name", intentObj)) + " prossimamente in edicola sono " + getBreak('short') + speechOutput;
            } else {
              speechOutput = "I numeri di " + pubName + " di " + getProsody(getSlotValue("author_name", intentObj)) + " prossimamente in edicola sono " + getBreak('short') + speechOutput;
            }
            speechOutput += getBreak("short") + " cosa vuoi fare ora?";
            if (supportsAPL(handlerInput)) {
              sessionAttributes.title = "Fumetti prossimamente in edicola";
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/publication_list_template.json'),
                  datasources: createDataSourceList(res, "Fumetti prossimamente in edicola")
                })
                .reprompt(globalRepromptText)
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt(globalRepromptText)
                .getResponse());
            }
          }
        } else {
          resolve(handlerInput.responseBuilder
            .speak("Non ho trovato " + pubName + " di " + getSlotValue("author_name", intentObj) + " prossimamente in uscita " + getBreak("short") + " cosa vuoi fare ora?")
            .reprompt(globalRepromptText)
            .getResponse());
        }
      });
    });
  }
};

const FindPubByPubNameAndYear = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindPubByPubNameAndYear'));
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const intentName = handlerInput.requestEnvelope.request.intent;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;
    let pubName = getSlotValue("comic_name", intentObj);
    let year = getSlotValue("year", intentObj);
    if(pubName === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo fumetto, ne conosco tanti altri, ad esempio Dylan Dog e Tex, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    if(year === undefined || isNaN(year)) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, sembra che tu non mi abbia detto l'anno in maniera corretta, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    sessionAttributes.pubName = pubName;
    sessionAttributes.year = year;
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublications/pubblicationName/" + pubName + "/year/" + year;
      httpGet(path, (res) => {
        sessionAttributes.publications = [];
        if (res !== undefined && res.length > 0) {
          if (res.length === 1) {
            let info = getAuthorInfo(res[0]);
            let speechOutput = "Il numero di " + pubName + " uscito nel " + year + " è ";
            speechOutput += "il numero " + res[0].number + " dal titolo " + getProsody(res[0].pubblicationTitle) + getBreak("short");
            speechOutput += " uscito il " + sayAsDate(res[0].pubblicationDate) + getBreak('short');
            speechOutput += info.speechOutput;
            speechOutput += " Vuoi sentire la sintesi della storia?" + getBreak('short');
            res[0].summary = cleanSummary(res[0].summary, info.authors)
            sessionAttributes.summary = res[0].summary;
            let cardAuthorInfo = info.cardAuthorInfo;

            if (supportsAPL(handlerInput)) {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/single_publication.json'),
                  datasources: createSingleDataSource(res[0], cardAuthorInfo, pubName + " uscito nel " + year)
                })
                .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(speechOutput)
                .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
                .getResponse());
            }
          } else {
            let speechOutput = "Ecco i fumetti di " + pubName + " usciti nel " + year + getBreak("short");
            for (var i = 0; i < res.length; i++) {
              if (i < maxResult) {
                speechOutput += " numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle) + getBreak("short");
              } else {
                sessionAttributes.publications.push({ pubblicationName: res[i].pubblicationName, number: res[i].number, pubblicationTitle: res[i].pubblicationTitle, pubblicationDate: res[i].pubblicationDate, smallCoverURL: res[i].smallCoverURL });
              }
            }
            if (res.length - maxResult > 0) {
              speechOutput += getBreak("short") + " Ci sono altri " + parseInt(res.length - maxResult) + " " + pubName + " usciti nel " + year + ", vuoi che te li dica?" + getBreak('medium');
              let reprompt = "Vuoi sentire gli altri " + parseInt(res.length - maxResult) + " " + pubName + " usciti nel " + year + "?";

              if (supportsAPL(handlerInput)) {
                sessionAttributes.title = pubName + " usciti nel " + year;
                resolve(handlerInput.responseBuilder
                  .speak(speechOutput)
                  .reprompt(reprompt)
                  .addDirective({
                    type: 'Alexa.Presentation.APL.RenderDocument',
                    document: require('./apl/publication_list_template.json'),
                    datasources: createDataSourceList(res, pubName + " usciti nel " + year)
                  })
                  .getResponse());
              } else {
                resolve(handlerInput.responseBuilder
                  .speak(speechOutput)
                  .reprompt(reprompt)
                  .getResponse());
              }
            } else {
              speechOutput += getBreak("short") + " cosa vuoi fare ora?";
              if (supportsAPL(handlerInput)) {
                sessionAttributes.title = pubName + " usciti nel " + year;
                resolve(handlerInput.responseBuilder
                  .speak(speechOutput)
                  .reprompt(globalRepromptText)
                  .addDirective({
                    type: 'Alexa.Presentation.APL.RenderDocument',
                    document: require('./apl/publication_list_template.json'),
                    datasources: createDataSourceList(res, pubName + " usciti nel " + year)
                  })
                  .getResponse());
              } else {
                resolve(handlerInput.responseBuilder
                  .speak(speechOutput)
                  .reprompt(globalRepromptText)
                  .getResponse());
              }
            }
          }
        } else {
          resolve(handlerInput.responseBuilder
            .speak("Non ho trovato " + pubName + " usciti nel " + year + " " + getBreak("short") + " cosa vuoi fare ora?")
            .reprompt(globalRepromptText)
            .getResponse());
        }
      });
    });
  }
};

async function findPublicationByAuthorAndYear(authorType, authorName, autorId, year, sessionAttributes, handlerInput) {
  let path = "/BonelliPubblications/rest/findPublications/" + authorType + "/" + autorId + "/year/" + year;
  return await new Promise((resolve, reject) => {
    (httpGet(path, (res) => {
      sessionAttributes.publications = [];
      sessionAttributes.previousIntent = "FindPubByAuthorAndYear";
      let speechOutput = "";
      var reprompt = "";
      var dataSource = undefined;
      let isMultiple = true;
      if (res !== undefined && res.length > 0) {
        var role = getAuthorRole(res[0], autorId);;
        if (res.length === 1) {
          isMultiple = false;
          let info = getAuthorInfo(res[0]);
          if (role !== undefined) {
            speechOutput = "Il fumetto " + getItalianRole(role, "single") + " nel " + year + " è " + getBreak('short');
          } else {
            speechOutput = "Il fumetto di " + authorName + " uscito nel " + year + " è " + getBreak('short');
          }
          speechOutput += "il " + res[0].pubblicationName + " numero " + res[0].number + " dal titolo " + getProsody(res[0].pubblicationTitle) + getBreak('short');
          speechOutput += " uscito il " + sayAsDate(res[0].pubblicationDate) + getBreak('short');
          speechOutput += info.speechOutput;
          speechOutput += " Vuoi sentire la sintesi della storia?" + getBreak('short');
          res[0].summary = cleanSummary(res[0].summary, info.authors)
          sessionAttributes.summary = res[0].summary;
          let cardAuthorInfo = info.cardAuthorInfo;
          reprompt = "Vuoi sentire la sintesi della storia?" + getBreak('short');
          if (supportsAPL(handlerInput)) {
            dataSource = createSingleDataSource(res[0], cardAuthorInfo, "Fumetti di " + authorName + " del " + year)
          }
        } else {
          for (var i = 0; i < res.length; i++) {
            if (i < maxResult) {
              speechOutput += res[i].pubblicationName + " numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle) + getBreak("short");
            } else {
              sessionAttributes.publications.push({ pubblicationName: res[i].pubblicationName, number: res[i].number, pubblicationTitle: res[i].pubblicationTitle, pubblicationDate: res[i].pubblicationDate, smallCoverURL: res[i].smallCoverURL });
            }
          }
          if (role !== undefined) {
            speechOutput = "I fumetti " + getItalianRole(role) + " da " + authorName + " nel " + year + " sono " + getBreak('short') + speechOutput;
          } else {
            speechOutput = "I fumetti di " + authorName + " pubblicati nel " + year + "sono " + getBreak('short') + speechOutput;
          }
          if (res.length - maxResult > 0) {
            speechOutput += getBreak("short") + " Ci sono altri " + parseInt(res.length - maxResult) + " fumetti pubblicati da " + authorName + ", vuoi che te li dica?" + getBreak('medium');
            reprompt = "Vuoi sentire gli altri " + parseInt(res.length - maxResult) + " fumetti pubblicati da " + authorName + "?";
          } else {
            speechOutput += getBreak("short") + " cosa vuoi fare ora?";
          }
          if (supportsAPL(handlerInput)) {
            dataSource = createDataSourceList(res, "Fumetti di " + authorName + " del " + year);
          }
        }
      } else {
        isMultiple = false;
        speechOutput = authorName + " nel " + year + " non ha pubblicato nessun fumetto " + getBreak("short") + " cosa vuoi fare ora?";
        reprompt = globalRepromptText;
      }
      resolve({
        isMultiple:isMultiple,
        speechOutput: speechOutput,
        reprompt: reprompt,
        dataSource: dataSource
      });
    }));
  });
}

const FindPubByAuthorAndYear = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindPubByAuthorAndYear'));
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const intentName = handlerInput.requestEnvelope.request.intent.name;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;
    let author = getAuthor(intentObj);
    let year = getSlotValue("year", intentObj);
    if(author === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo autore, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    if(year === undefined || isNaN(year)) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, sembra che tu non mi abbia detto l'anno in maniera corretta, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    sessionAttributes.year = year;
    sessionAttributes.authorName = getSlotValue("author_name", intentObj);
    var authorName = getSlotValue("author_name", intentObj);
    return new Promise((resolve) => {
      findPublicationByAuthorAndYear(author.type, authorName, author.id, year, sessionAttributes, handlerInput)
        .then(function (retObj) {
          if (supportsAPL(handlerInput) && retObj.dataSource !== undefined) {
            let template = "./apl/publication_list_template.json";
            if(!retObj.isMultiple) {
              template = './apl/single_publication.json';
            }
            sessionAttributes.title = "Fumetti di " + authorName + " del " + year;
            resolve(handlerInput.responseBuilder
              .speak(retObj.speechOutput)
              .reprompt(retObj.reprompt)
              .addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                document: require(template),
                datasources: retObj.dataSource
              })
              .getResponse());
          } else {
            resolve(handlerInput.responseBuilder
              .speak(retObj.speechOutput)
              .reprompt(retObj.reprompt)
              .getResponse());
          }
        },
          function (error) {
            console.log("FindPubByAuthorAndYear", error);
            resolve(handlerInput.responseBuilder
              .speak("Mi dispiace, ho avuto un problema nel soddifare la tua richiesta, prova a farmi un'altra domanda")
              .reprompt(globalRepromptText)
              .getResponse());
          });
    });
  }
};

const FindPubByPubNameAndNumber = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindPubByPubNameAndNumber'));
  },
  handle(handlerInput) {
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const intentName = handlerInput.requestEnvelope.request.intent.name;
    sessionAttributes.previousIntent = intentName;
    let pubName = getSlotValue("comic_name", intentObj);
    let number = getSlotValue("comic_number", intentObj);
    if(pubName === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo fumetto, ne conosco tanti altri, ad esempio Dylan Dog e Tex, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    if(number === undefined || isNaN(number)) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, sembra che tu non mi abbia detto il numero del fumetto che cerchi in maniera corretta, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findPublication/pubblicationName/" + pubName + "/pubblicationNumber/" + number;
      var cardAuthorInfo = "";
      httpGet(path, (res) => {
        let speechOutput = "";
        let cardAuthorInfo = "";
        if (res !== undefined && res.number !== -1) {
          speechOutput += "Il titolo del " + res.pubblicationName + " numero " + getProsody(res.number) + " è " + getBreak('short') + getProsody(res.pubblicationTitle) + getBreak('short');
          let info = getAuthorInfo(res);
          speechOutput += " uscito il " + sayAsDate(res.pubblicationDate) + getBreak('short');
          speechOutput += info.speechOutput;
          cardAuthorInfo += info.cardAuthorInfo;
          speechOutput += " Vuoi sentire la sintesi della storia?" + getBreak('short');
          res.summary = cleanSummary(res.summary, info.authors)
          sessionAttributes.summary = res.summary;
          if (supportsAPL(handlerInput)) {
            resolve(handlerInput.responseBuilder
              .speak(speechOutput)
              .addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                document: require('./apl/single_publication.json'),
                datasources: createSingleDataSource(res, cardAuthorInfo, "Catalogo Bonelli")
              })
              .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
              .getResponse());
          } else {
            resolve(handlerInput.responseBuilder
              .speak(speechOutput)
              .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
              .getResponse());
          }
        } else {
          speechOutput = "Non ho trovato il " + pubName + " numero " + number + " " + getBreak("short") + " cosa vuoi fare ora?";
          resolve(handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt(globalRepromptText)
            .getResponse());
        }
      });
    });
  }
};

async function findPublicationByAuthor(authorType, authorId, authorName, sessionAttributes, handlerInput) {
  return await new Promise((resolve, reject) => {
    let path = "/BonelliPubblications/rest/findPublications/" + authorType + "/" + authorId;
    httpGet(path, (res) => {
      sessionAttributes.publications = [];
      sessionAttributes.previousIntent = "FindPubByAuthor";
      var role = undefined;
      let speechOutput = "";
      let reprompt = "";
      let dataSource = undefined;
      let isMultiple = true;
      if (res !== undefined && res.length > 0) {
        if (res.length === 1) {
          isMultiple = false;
          let info = getAuthorInfo(res[0]);
          if (role !== undefined) {
            speechOutput = "Il fumetto " + getItalianRole(role, "single") + " da " + authorName + " è " + getBreak("short");
          } else {
            speechOutput = "Il fumetto di " + authorName + " è " + getBreak("short");
          }
          speechOutput += "il " + res[0].pubblicationName + " numero " + res[0].number + " dal titolo " + getProsody(res[0].pubblicationTitle) + getBreak('short');
          speechOutput += " uscito il " + sayAsDate(res[0].pubblicationDate) + getBreak('short');
          speechOutput += info.speechOutput;
          speechOutput += " Vuoi sentire la sintesi della storia?" + getBreak('short');
          res[0].summary = cleanSummary(res[0].summary, info.authors)
          sessionAttributes.summary = res[0].summary;

          reprompt = "Vuoi sentire la sintesi della storia?" + getBreak('short');
          if (supportsAPL(handlerInput)) {
            let cardAuthorInfo = info.cardAuthorInfo;
            dataSource = createSingleDataSource(res[0], cardAuthorInfo, "Fumetti di " + authorName);
          }
        } else {
          var max = ((res.length <= maxStoreResultInSession) ? res.length : maxStoreResultInSession);
          for (var i = 0; i < max; i++) {
            labelAuthor: for (var j = 0; j < res[i].pubblicationAuthors.length; j++) {
              if (res[i].pubblicationAuthors[j].pk.author.nameSurname === authorId ||
                res[i].pubblicationAuthors[j].pk.author.surname === authorId) {
                role = res[i].pubblicationAuthors[j].pk.role;
                break labelAuthor;
              }
            }
            if (i < maxResult) {
              speechOutput += res[i].pubblicationName + " numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle) + getBreak("short");
            } else {
              sessionAttributes.publications.push({ pubblicationName: res[i].pubblicationName, number: res[i].number, pubblicationTitle: res[i].pubblicationTitle, pubblicationDate: res[i].pubblicationDate, smallCoverURL: res[i].smallCoverURL });
            }
          }
          if (role !== undefined) {
            speechOutput = "I fumetti " + getItalianRole(role) + " da " + authorName + " sono " + getBreak("short") + speechOutput;
          } else {
            speechOutput = "I fumetti di " + authorName + " sono " + getBreak("short") + speechOutput;
          }
          if (parseInt(res.length - maxResult) > 0) {
            speechOutput += " Ci sono altri " + parseInt(res.length - maxResult) + " fumetti di " + authorName + " in elenco, vuoi che te li dica?" + getBreak('medium');
            reprompt = " Vuoi sentire gli altri " + parseInt(res.length - maxResult) + " fumetti di " + authorName + "?";
          }
          if (reprompt === '') {
            speechOutput += getBreak("short") + " cosa vuoi fare ora?";
            reprompt = globalRepromptText;
            if (supportsAPL(handlerInput)) {
              sessionAttributes.title = "Fumetti di " + authorName;
              dataSource = createDataSourceList(res, "Fumetti di " + authorName);
            }
          } else {
            if (supportsAPL(handlerInput)) {
              sessionAttributes.title = "Fumetti di " + authorName;
              dataSource = createDataSourceList(res, "Fumetti di " + authorName);
            }
          }
        }
      } else {
        isMultiple = false;
        speechOutput = "Non ho trovato fumetti di " + authorName + " " + getBreak("short") + " cosa vuoi fare ora?";
        reprompt = globalRepromptText;
      }
      resolve({
        isMultiple: isMultiple,
        speechOutput: speechOutput,
        reprompt: reprompt,
        dataSource: dataSource
      });
    });
  });
}

const FindPubByAuthor = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindPubByAuthor'));
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const intentName = handlerInput.requestEnvelope.request.intent.name;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;

    let author = getAuthor(intentObj);
    if(author === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo autore, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    let authorName = getSlotValue("author_name", intentObj);
    sessionAttributes.authorName = authorName
    return new Promise((resolve) => {
      findPublicationByAuthor(author.type, author.id, authorName, sessionAttributes, handlerInput)
        .then(function (retObj) {
          if (supportsAPL(handlerInput) && retObj.dataSource !== undefined) {
            let template = "./apl/publication_list_template.json";
            if(!retObj.isMultiple) {
              template = './apl/single_publication.json';
            }
            sessionAttributes.title = "Fumetti pubblicati da " + getSlotValue("author_name", intentObj);
            resolve(handlerInput.responseBuilder
              .speak(retObj.speechOutput)
              .reprompt(retObj.reprompt)
              .addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                document: require(template),
                datasources: retObj.dataSource
              })
              .getResponse());
          } else {
            resolve(handlerInput.responseBuilder
              .speak(retObj.speechOutput)
              .reprompt(retObj.reprompt)
              .getResponse());
          }
        },
          function (error) {
            console.log("FindPubByAuthor", error);
            resolve(handlerInput.responseBuilder
              .speak("Mi dispiace, ho avuto un problema nel soddifare la tua richiesta, prova a farmi un'altra domanda")
              .reprompt(globalRepromptText)
              .getResponse());
          });
    });
  }
};

async function findPublicationByAuthorAndPubName(authorType, authorId, authorName, pubName, sessionAttributes, handlerInput) {
  return await new Promise((resolve, reject) => {
    let path = "/BonelliPubblications/rest/findPublications/" + authorType + "/" + authorId + "/pubblicationName/" + pubName;
    httpGet(path, (res) => {
      sessionAttributes.publications = [];
      sessionAttributes.previousIntent = "FindPubByAuthorAndPubName";
      let speechOutput = "";
      let reprompt = "";
      let dataSource = undefined;
      let isMultiple = true;
      if (res !== undefined && res.length > 0) {
        var role = getAuthorRole(res[0], authorId);
        if (res.length === 1) {
          isMultiple = false;
          let info = getAuthorInfo(res[0]);
          if (role !== undefined) {
            speechOutput = "Il " + pubName + " " + getItalianRole(role, "single") + " da " + authorName;
          } else {
            speechOutput = "Il " + pubName + " di " + authorName;
          }
          speechOutput += "è numero " + res[0].number + " dal titolo " + getProsody(res[0].pubblicationTitle) + getBreak('short');
          speechOutput += " uscito il " + sayAsDate(res[0].pubblicationDate) + getBreak('short');
          speechOutput += info.speechOutput;
          speechOutput += " Vuoi sentire la sintesi della storia?" + getBreak('short');
          res[0].summary = cleanSummary(res[0].summary, info.authors)
          sessionAttributes.summary = res[0].summary;
          reprompt = "Vuoi sentire la sintesi della storia?" + getBreak('short');
          if (supportsAPL(handlerInput)) {
            let cardAuthorInfo = info.cardAuthorInfo;
            dataSource = createSingleDataSource(res[0], cardAuthorInfo, pubName + " di " + authorName);
          }
        } else {
          var max = ((res.length <= maxStoreResultInSession) ? res.length : maxStoreResultInSession);
          for (var i = 0; i < max; i++) {
            if (i < maxResult) {
              speechOutput += " il numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle) + getBreak("short");
            } else {
              sessionAttributes.publications.push({ pubblicationName: res[i].pubblicationName, number: res[i].number, pubblicationTitle: res[i].pubblicationTitle, pubblicationDate: res[i].pubblicationDate, smallCoverURL: res[i].smallCoverURL });
            }
          }
          if (role !== undefined) {
            speechOutput = "I numeri di " + pubName + " " + getItalianRole(role) + " da " + getProsody(authorName) + " sono " + getBreak('short') + speechOutput;
          } else {
            speechOutput = "I numeri di " + pubName + " di " + getProsody(authorName) + " sono " + getBreak('short') + speechOutput;
          }
          if (res.length - maxResult > 0) {
            speechOutput += getBreak("short") + " Ci sono altri " + parseInt(res.length - maxResult) + " fumetti pubblicati da " + authorName + ", vuoi che te li dica?" + getBreak('medium');
            reprompt = "Vuoi sentire gli altri " + parseInt(res.length - maxResult) + " fumetti pubblicati da " + authorName + "?";
            if (supportsAPL(handlerInput)) {
              sessionAttributes.title = "Fumetti di " + pubName + " pubblicati da " + authorName;
              dataSource = createDataSourceList(res, "Fumetti di " + pubName + " pubblicati da " + authorName);
            }
          } else {
            speechOutput += getBreak("short") + " cosa vuoi fare ora?";
            if (supportsAPL(handlerInput)) {
              sessionAttributes.title = "Fumetti di " + pubName + " pubblicati da " + authorName;
              dataSource = createDataSourceList(res, "Fumetti di " + pubName + " pubblicati da " + authorName);
              reprompt = globalRepromptText;
            }
          }
        }
      } else {
        isMultiple = false;
        speechOutput = "Non ho trovato fumetti di " + pubName + " di " + getProsody(authorName) + " " + getBreak("short") + " cosa vuoi fare ora?";
        reprompt = globalRepromptText;
      }
      resolve({
        isMultiple: isMultiple,
        speechOutput: speechOutput,
        reprompt: reprompt,
        dataSource: dataSource
      });
    });
  });
}

const FindPubByAuthorAndPubName = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'FindPubByAuthorAndPubName');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;
    let author = getAuthor(intentObj);
    let pubName = getSlotValue("comic_name", intentObj);
    if(author === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo autore, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    if(pubName === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo fumetto, ne conosco tanti altri, ad esempio Dylan Dog e Tex, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    let authorName = getSlotValue("author_name", intentObj);

    sessionAttributes.authorName = getSlotValue("author_name", intentObj);
    sessionAttributes.pubName = pubName;
    sessionAttributes.authorName = authorName;
    return new Promise((resolve) => {
      findPublicationByAuthorAndPubName(author.type, author.id, authorName, pubName, sessionAttributes, handlerInput)
        .then(function (retObj) {
          if (supportsAPL(handlerInput) && retObj.dataSource !== undefined) {
            let template = './apl/publication_list_template.json';
            if(!retObj.isMultiple) {
              template = "./apl/single_publication.json";
            }
            sessionAttributes.title = "Fumetti di " + pubName + " pubblicati da " + getSlotValue("author_name", intentObj);
            resolve(handlerInput.responseBuilder
              .speak(retObj.speechOutput)
              .reprompt(retObj.reprompt)
              .addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                document: require(template),
                datasources: retObj.dataSource
              })
              .getResponse());
          } else {
            resolve(handlerInput.responseBuilder
              .speak(retObj.speechOutput)
              .reprompt(retObj.reprompt)
              .getResponse());
          }
        },
          function (error) {
            console.log("FindPubByAuthorAndPubName", error);
            resolve(handlerInput.responseBuilder
              .speak("Mi dispiace, ho avuto un problema nel soddifare la tua richiesta, prova a farmi un'altra domanda")
              .reprompt(globalRepromptText)
              .getResponse());
          });
    });
  }
};

async function findPublicationByAuthorAndPubNameAndYear(authorType, authorId, authorName, pubName, year, sessionAttributes, handlerInput) {
  return await new Promise((resolve, reject) => {
    let path = "/BonelliPubblications/rest/findPublications/" + authorType + "/" + authorId + "/pubblicationName/" + pubName + "/year/" + year;
    let speechOutput = "";
    let reprompt = "";
    let dataSource = undefined;
    let isMultiple = true;
    httpGet(path, (res) => {
      sessionAttributes.publications = [];
      sessionAttributes.previousIntent = "FindPubByAuthorAndPubNameAndYear";
      let speechOutput = "";
      if (res !== undefined && res.length > 0) {
        var role = getAuthorRole(res[0], authorId);
        if (res.length === 1) {
          isMultiple = false;
          let info = getAuthorInfo(res[0]);
          if (role !== undefined) {
            speechOutput = "Il numero di " + pubName + " " + getItalianRole(role, "single") + " da " + authorName + " nel " + year + getBreak('short');
          } else {
            speechOutput = "Il numero di " + pubName + " di " + authorName + " pubblicato nel " + year + getBreak('short');
          }
          speechOutput += " è il " + res[0].number + " dal titolo " + getProsody(res[0].pubblicationTitle) + getBreak('short');
          speechOutput += " uscito il " + sayAsDate(res[0].pubblicationDate) + getBreak('short');
          speechOutput += info.speechOutput;
          speechOutput += " Vuoi sentire la sintesi della storia?" + getBreak('short');
          res[0].summary = cleanSummary(res[0].summary, info.authors)
          sessionAttributes.summary = res[0].summary;

          reprompt = "Vuoi sentire la sintesi della storia?" + getBreak('short');
          if (supportsAPL(handlerInput)) {
            let cardAuthorInfo = info.cardAuthorInfo;
            dataSource = createSingleDataSource(res[0], cardAuthorInfo, pubName + " di " + authorName);
          }
        } else {
          for (var i = 0; i < res.length; i++) {
            if (i < maxResult) {
              speechOutput += " il numero " + res[i].number + " dal titolo " + getProsody(res[i].pubblicationTitle) + getBreak("short");
            } else {
              sessionAttributes.publications.push({ pubblicationName: res[i].pubblicationName, number: res[i].number, pubblicationTitle: res[i].pubblicationTitle, pubblicationDate: res[i].pubblicationDate, smallCoverURL: res[i].smallCoverURL });
            }
          }
          if (role !== undefined) {
            speechOutput = "Il fumetti di " + pubName + " " + getItalianRole(role) + " da " + authorName + " usciti nell'anno " + year + " sono " + getBreak("short") + speechOutput;
          } else {
            speechOutput = "Il fumetti di " + pubName + " di " + authorName + " usciti nell'anno " + year + " sono " + getBreak("short") + speechOutput;
          }
          if (res.length - maxResult > 0) {
            speechOutput += getBreak("short") + " Ci sono altri " + parseInt(res.length - maxResult) + " fumetti di " + authorName + " usciti nel " + year + ", vuoi che te li dica?" + getBreak('medium');
            reprompt = "Vuoi sentire gli altri " + parseInt(res.length - maxResult) + " fumetti pubblicati da " + authorName + "nel " + year + "?";

            if (supportsAPL(handlerInput)) {
              sessionAttributes.title = "Fumetti di " + pubName + " pubblicati da " + authorName + " nel " + year;
              dataSource = createDataSourceList(res, "Fumetti di " + pubName + " pubblicati da " + authorName + " nel " + year);
            }
          } else {
            speechOutput += getBreak("short") + " cosa vuoi fare ora?";
            reprompt = globalRepromptText;
            if (supportsAPL(handlerInput)) {
              sessionAttributes.title = "Fumetti di " + pubName + " pubblicati da " + authorName + " nel " + year;
              dataSource = createDataSourceList(res, "Fumetti di " + pubName + " pubblicati da " + authorName + " nel " + year);
            }
          }
        }
      } else {
        isMultiple = false;
        speechOutput = "Non ho trovato fumetti di " + pubName + " di " + authorName + " usciti nell'anno " + year + " " + getBreak("short") + " cosa vuoi fare ora?";
        reprompt = globalRepromptText;
      }
      resolve({
        isMultiple: isMultiple,
        speechOutput: speechOutput,
        reprompt: reprompt,
        dataSource: dataSource
      });
    });
  });
}

const FindPubByAuthorAndPubNameAndYear = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'FindPubByAuthorAndPubNameAndYear');
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;

    let author = getAuthor(intentObj);
    if(author === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo autore, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    let authorName = getSlotValue("author_name", intentObj);
    let pubName = getSlotValue("comic_name", intentObj);
    if(pubName === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo fumetto, ne conosco tanti altri, ad esempio Dylan Dog e Tex, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    let year = getSlotValue("year", intentObj);
    if(year === undefined || isNaN(year)) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, sembra che tu non mi abbia detto l'anno in maniera corretta, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    sessionAttributes.authorName = authorName;
    sessionAttributes.pubName = pubName;
    sessionAttributes.year = year;

    return new Promise((resolve) => {
      findPublicationByAuthorAndPubNameAndYear(author.type, author.id, authorName, pubName, year, sessionAttributes, handlerInput)
        .then(function (retObj) {
          if (supportsAPL(handlerInput) && retObj.dataSource !== undefined) {
            let template = './apl/publication_list_template.json';
            if(!retObj.isMultiple) {
              template = './apl/single_publication.json';
            }
            sessionAttributes.title = "Fumetti di " + pubName + " pubblicati da " + authorName + " nel " + year;
            resolve(handlerInput.responseBuilder
              .speak(retObj.speechOutput)
              .reprompt(retObj.reprompt)
              .addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                document: require(template),
                datasources: retObj.dataSource
              })
              .getResponse());
          } else {
            resolve(handlerInput.responseBuilder
              .speak(retObj.speechOutput)
              .reprompt(retObj.reprompt)
              .getResponse());
          }
        },
          function (error) {
            console.log("FindPubByAuthorAndPubNameAndYear", error);
            resolve(handlerInput.responseBuilder
              .speak("Mi dispiace, ho avuto un problema nel soddifare la tua richiesta, prova a farmi un'altra domanda")
              .reprompt(globalRepromptText)
              .getResponse());
          });
    });
  }
};

const FindPubByOrdinalAndPubName = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindLastPubByPubName' || request.intent.name === 'FindPubByOrdinalAndPubName'));
  },
  handle(handlerInput) {
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const intentName = handlerInput.requestEnvelope.request.intent.name;
    sessionAttributes.previousIntent = intentName;
    let pubName = getSlotValue("comic_name", intentObj);
    if(pubName === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo fumetto, ne conosco tanti altri, ad esempio Dylan Dog e Tex, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    let ordinal = getSlotValue("ordinal", intentObj);
    if(ordinal === undefined || isNaN(ordinal)) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, sembra che tu non mi abbia detto il numero del fumetto che cerchi in maniera corretta, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findLastPublication/pubblicationName/" + pubName;
      if (intentName === "FindPubByOrdinalAndPubName") {
        path = "/BonelliPubblications/rest/findPublication/pubblicationName/" + pubName + "/ordinal/" + ordinal;
      }
      var cardAuthorInfo = "";
      httpGet(path, (res) => {
        let speechOutput = "";
        let cardAuthorInfo = "";
        if (res !== undefined && res.number !== -1) {
          if (intentName === "FindPubByOrdinalAndPubName") {
            speechOutput += "Il titolo del " + sayAsOrdinal(ordinal) + " " + res.pubblicationName + " è " + getBreak('short') + getProsody(res.pubblicationTitle) + getBreak('short');
          } else {
            speechOutput += "Il titolo dell'ultimo " + res.pubblicationName + " numero " + getProsody(res.number) + " è " + getBreak('short') + getProsody(res.pubblicationTitle) + getBreak('short');
          }
          let info = getAuthorInfo(res);
          speechOutput += " uscito il " + sayAsDate(res.pubblicationDate) + getBreak('short');
          speechOutput += info.speechOutput;
          cardAuthorInfo += info.cardAuthorInfo;

          speechOutput += " Vuoi sentire la sintesi della storia?" + getBreak('short');
          res.summary = cleanSummary(res.summary, info.authors)
          sessionAttributes.summary = res.summary;
          if (supportsAPL(handlerInput)) {
            resolve(handlerInput.responseBuilder
              .speak(speechOutput)
              .addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                document: require('./apl/single_publication.json'),
                datasources: createSingleDataSource(res, cardAuthorInfo, "Catalogo Bonelli")
              })
              .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
              .getResponse());
          } else {
            resolve(handlerInput.responseBuilder
              .speak(speechOutput)
              .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
              .getResponse());
          }
        } else {
          if (intentName === "FindPubByOrdinalAndPubName") {
            speechOutput = "Non ho trovato il " + sayAsOrdinal(ordinal) + " " + pubName + " " + getBreak("short") + " cosa vuoi fare ora?";
          } else {
            speechOutput = "Non ho trovato l'ultimo " + pubName + " uscito " + getBreak("short") + " cosa vuoi fare ora?";
          }
          resolve(handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt(globalRepromptText)
            .getResponse());
        }
      });
    });
  }
};

const FindPubByOrdinalAndPubNameAndAuthName = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindLastPubByPubNameAuthName' || request.intent.name === 'FindPubByOrdinalAndPubNameAndAuthName'));
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    var intentName = intentObj.name;
    sessionAttributes.previousIntent = intentObj.name;
    let author = getAuthor(intentObj);
    let pubName = getSlotValue("comic_name", intentObj);
    let ordinal = getSlotValue("ordinal", intentObj);
    
    if(author === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo autore, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    if(pubName === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo fumetto, ne conosco tanti altri, ad esempio Dylan Dog e Tex, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    if(ordinal === undefined || isNaN(ordinal)) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, sembra che tu non mi abbia detto il numero del fumetto che cerchi in maniera corretta, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    
    sessionAttributes.authorName = getSlotValue("author_name", intentObj);
    sessionAttributes.pubName = pubName;

    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findLastPublication/" + author.type + "/" + author.id + "/pubblicationName/" + pubName;
      if (intentName === "FindPubByOrdinalAndPubNameAndAuthName") {
        path = "/BonelliPubblications/rest/findPublication/" + author.type + "/" + author.id + "/pubblicationName/" + pubName + "/ordinal/" + ordinal;
      }
      httpGet(path, (res) => {
        sessionAttributes.publications = [];
        var reprompt = "";
        let speechOutput = "";
        if (res !== undefined && res.number !== -1) {
          var role = getAuthorRole(res, author.id);
          let info = getAuthorInfo(res);
          if (role !== undefined) {
            if (intentName === "FindPubByOrdinalAndPubNameAndAuthName") {
              speechOutput += "Il titolo del  " + sayAsOrdinal(ordinal) + " " + pubName + " " + getItalianRole(role, "single") + " da " + getSlotValue("author_name", intentObj);
              speechOutput += "è " + getProsody(res.pubblicationTitle) + getBreak('short');
            } else {
              speechOutput = "L'ultimo " + pubName + " " + getItalianRole(role, "single") + " da " + getSlotValue("author_name", intentObj);
              speechOutput += "è " + getProsody(res.pubblicationTitle) + getBreak('short');
            }
          } else {
            if (intentName === "FindPubByOrdinalAndPubNameAndAuthName") {
              speechOutput += "Il titolo del  " + sayAsOrdinal(ordinal) + " " + pubName + " di " + getSlotValue("author_name", intentObj);
              speechOutput += "è il numero " + res.number + " dal titolo " + getProsody(res.pubblicationTitle) + getBreak('short');
            } else {
              speechOutput = "L'ultimo " + pubName + " di " + getSlotValue("author_name", intentObj);
              speechOutput += "è il numero " + res.number + " dal titolo " + getProsody(res.pubblicationTitle) + getBreak('short');
            }
          }
          speechOutput += " uscito il " + sayAsDate(res.pubblicationDate) + getBreak('short');
          speechOutput += info.speechOutput;
          speechOutput += " Vuoi sentire la sintesi della storia?" + getBreak('short');
          res.summary = cleanSummary(res.summary, info.authors)
          sessionAttributes.summary = res.summary;
          let cardAuthorInfo = info.cardAuthorInfo;
          if (supportsAPL(handlerInput)) {
            resolve(handlerInput.responseBuilder
              .speak(speechOutput)
              .addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                document: require('./apl/single_publication.json'),
                datasources: createSingleDataSource(res, cardAuthorInfo, pubName + " di " + getSlotValue("author_name", intentObj))
              })
              .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
              .getResponse());
          } else {
            resolve(handlerInput.responseBuilder
              .speak(speechOutput)
              .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
              .getResponse());
          }
        } else {
          if (intentName === "FindPubByOrdinalAndPubNameAndAuthName") {
            speechOutput = "Non ho trovato il " + sayAsOrdinal(ordinal) + " fumetto di " + pubName + " di " + getProsody(getSlotValue("author_name", intentObj)) + " " + getBreak("medium") + " cosa vuoi fare ora?";
          } else {
            speechOutput = "Non ho trovato l'ultimo fumetto di " + pubName + " di " + getProsody(getSlotValue("author_name", intentObj)) + " " + getBreak("short") + " cosa vuoi fare ora?";
          }
          resolve(handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt(globalRepromptText)
            .getResponse());
        }
      });
    });
  }
};

const FindPubByOrdinalAndAuthName = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && (request.intent.name === 'FindLastPubByAuthName' || request.intent.name === 'FindPubByOrdinalAndAuthName'));
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    const intentName = intentObj.name;
    sessionAttributes.previousIntent = intentObj.name;
    let author = getAuthor(intentObj);
    let ordinal = getSlotValue("ordinal", intentObj);
    if(author === undefined) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, non conosco questo autore, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    if(ordinal === undefined || isNaN(ordinal)) {
      return handlerInput.responseBuilder
      .speak("Mi dispiace, sembra che tu non mi abbia detto il numero del fumetto che cerchi in maniera corretta, prova a farmi nuovamente questa domanda")
      .reprompt(globalRepromptText)
      .getResponse();
    }
    sessionAttributes.authorName = getSlotValue("author_name", intentObj);

    return new Promise((resolve) => {
      let path = "/BonelliPubblications/rest/findLastPublication/" + author.type + "/" + author.id;
      if (intentName === 'FindPubByOrdinalAndAuthName') {
        path = "/BonelliPubblications/rest/findPublication/" + author.type + "/" + author.id + "/ordinal/" + ordinal;
      }
      httpGet(path, (res) => {
        sessionAttributes.publications = [];
        var reprompt = "";
        let speechOutput = "";
        if (res !== undefined && res.number !== -1) {
          var role = getAuthorRole(res, author.id);
          let info = getAuthorInfo(res);
          if (role !== undefined) {
            if (intentName === 'FindPubByOrdinalAndAuthName') {
              speechOutput = "Il titolo del " + sayAsOrdinal(ordinal) + " fumetto " + getItalianRole(role, "single") + " da " + getSlotValue("author_name", intentObj);
              speechOutput += " è " + getProsody(res.pubblicationTitle) + getBreak('short');
            } else {
              speechOutput = "L'ultimo fumetto " + getItalianRole(role, "single") + " da " + getSlotValue("author_name", intentObj);
              speechOutput += " è il " + res.pubblicationName + " numero " + res.number + " dal titolo " + getProsody(res.pubblicationTitle) + getBreak('short');
            }
          } else {
            if (intentName === 'FindPubByOrdinalAndAuthName') {
              speechOutput = "Il titolo del " + sayAsOrdinal(ordinal) + " fumetto di " + getSlotValue("author_name", intentObj);
              speechOutput += " è " + getProsody(res.pubblicationTitle) + getBreak('short');
            } else {
              speechOutput = "L'ultimo fumetto di " + getSlotValue("author_name", intentObj);
              speechOutput += " è il " + res.pubblicationName + " numero " + res.number + " dal titolo " + getProsody(res.pubblicationTitle) + getBreak('short');
            }
          }
          speechOutput += " uscito il " + sayAsDate(res.pubblicationDate) + getBreak('short');
          speechOutput += info.speechOutput;
          speechOutput += " Vuoi sentire la sintesi della storia?" + getBreak('short');
          res.summary = cleanSummary(res.summary, info.authors)
          sessionAttributes.summary = res.summary;
          let cardAuthorInfo = info.cardAuthorInfo;
          if (supportsAPL(handlerInput)) {
            resolve(handlerInput.responseBuilder
              .speak(speechOutput)
              .addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                document: require('./apl/single_publication.json'),
                datasources: createSingleDataSource(res, cardAuthorInfo, getSlotValue("author_name", intentObj))
              })
              .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
              .getResponse());
          } else {
            resolve(handlerInput.responseBuilder
              .speak(speechOutput)
              .reprompt("Vuoi sentire la sintesi della storia?" + getBreak('short'))
              .getResponse());
          }
        } else {
          if (intentName === 'FindPubByOrdinalAndAuthName') {
            speechOutput = "Non ho trovato il " + sayAsOrdinal(ordinal) + " fumetto pubblicato da " + getProsody(getSlotValue("author_name", intentObj)) + " " + getBreak("short") + " cosa vuoi fare ora?";
          } else {
            speechOutput = "Non ho trovato l'ultimo fumetto pubblicato da " + getProsody(getSlotValue("author_name", intentObj)) + " " + getBreak("short") + " cosa vuoi fare ora?";
          }
          resolve(handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt(globalRepromptText)
            .getResponse());
        }
      });
    });
  }
};
/*
const AdditionalInfo = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest' &&
      request.intent.name === 'AdditionalInfo')
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    const intentName = intentObj.name;
    const previousIntent = sessionAttributes.previousIntent;
    
    let speechOutput = "";
    switch (previousIntent) {
      case 'CountPubByAuthor':
        sessionAttributes.previousIntent = intentObj.name;
        return new Promise((resolve) => {
          findPublicationByAuthor(sessionAttributes.authorType, sessionAttributes.authorId, sessionAttributes.authorName, sessionAttributes, handlerInput)
          .then(function (retObj) {
            if (supportsAPL(handlerInput) && retObj.dataSource !== undefined) {
              sessionAttributes.title = "Fumetti di " + sessionAttributes.authorName;
              resolve(handlerInput.responseBuilder
                .speak(retObj.speechOutput)
                .reprompt(retObj.reprompt)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/publication_list_template.json'),
                  datasources: retObj.dataSource
                })
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(retObj.speechOutput)
                .reprompt(retObj.reprompt)
                .getResponse());
            }
        },
          function (error) {
            console.log("Si è verificato l'errore " + error.message);
          });
      });
      case 'CountPubByAuthorAndYear':
        sessionAttributes.previousIntent = intentObj.name;
        return new Promise((resolve) => {
          findPublicationByAuthorAndYear(sessionAttributes.authorType, sessionAttributes.authorName, sessionAttributes.authorId, sessionAttributes.year, sessionAttributes, handlerInput)
          .then(function (retObj) {
            if (supportsAPL(handlerInput) && retObj.dataSource !== undefined) {
              sessionAttributes.title = "Fumetti di " + sessionAttributes.authorName + " del " + sessionAttributes.year;
              resolve(handlerInput.responseBuilder
                .speak(retObj.speechOutput)
                .reprompt(retObj.reprompt)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/publication_list_template.json'),
                  datasources: retObj.dataSource
                })
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(retObj.speechOutput)
                .reprompt(retObj.reprompt)
                .getResponse());
            }
        },
          function (error) {
            console.log("Si è verificato l'errore " + error.message);
          });
      });
      case 'CountPubByPubNameAndAuthor':
        sessionAttributes.previousIntent = intentObj.name;
        return new Promise((resolve) => {
          findPublicationByAuthorAndPubName(sessionAttributes.authorType, sessionAttributes.authorId, sessionAttributes.authorName, sessionAttributes.pubName, sessionAttributes, handlerInput)
          .then(function (retObj) {
            if (supportsAPL(handlerInput) && retObj.dataSource !== undefined) {
              sessionAttributes.title = sessionAttributes.pubName + " di " + sessionAttributes.authorName;
              resolve(handlerInput.responseBuilder
                .speak(retObj.speechOutput)
                .reprompt(retObj.reprompt)
                .addDirective({
                  type: 'Alexa.Presentation.APL.RenderDocument',
                  document: require('./apl/publication_list_template.json'),
                  datasources: retObj.dataSource
                })
                .getResponse());
            } else {
              resolve(handlerInput.responseBuilder
                .speak(retObj.speechOutput)
                .reprompt(retObj.reprompt)
                .getResponse());
            }
        },
          function (error) {
            console.log("Si è verificato l'errore " + error.message);
          });
      });
      case 'CountPubByPubNameAndAuthorAndYear':
        return new Promise((resolve) => {
      findPublicationByAuthorAndPubNameAndYear(sessionAttributes.authorType, sessionAttributes.authorId, sessionAttributes.authorName, sessionAttributes.pubName, sessionAttributes.year, sessionAttributes, handlerInput)
        .then(function (retObj) {
          if (supportsAPL(handlerInput) && retObj.dataSource !== undefined) {
                sessionAttributes.title = "Fumetti di " + sessionAttributes.pubName + " pubblicati da " + sessionAttributes.authorName +" nel "+sessionAttributes.year;
                resolve(handlerInput.responseBuilder
                  .speak(retObj.speechOutput)
                  .reprompt(retObj.reprompt)
                  .addDirective({
                    type: 'Alexa.Presentation.APL.RenderDocument',
                    document: require('./apl/publication_list_template.json'),
                    datasources: retObj.dataSource
                  })
                  .getResponse());
              } else {
                resolve(handlerInput.responseBuilder
                  .speak(retObj.speechOutput)
                  .reprompt(retObj.reprompt)
                  .getResponse());
              }
      },
        function (error) {
          console.log("Si è verificato l'errore " + error.message);
        });
    });
      default:
        speechOutput = "Forse non ho capito cosa mi chiedi, riprova";
        return handlerInput.responseBuilder
          .speak(speechOutput)
          .reprompt(globalRepromptText)
          .getResponse();
    }
  }
};
*/
const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    console.log("error", error);
    return handlerInput.responseBuilder
      .speak('Mi dispiace, si è verificato un errore')
      .withShouldEndSession(true)
      .getResponse();
  }
};

const StopIntentHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest' &&
      (request.intent.name === 'AMAZON.StopIntent' ||
        request.intent.name === 'AMAZON.CancelIntent'))
  },
  handle(handlerInput) {
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    const intentName = handlerInput.requestEnvelope.request.intent.name;
    sessionAttributes.previousIntent = intentName;
    return handlerInput.responseBuilder
      .speak('Ciao a presto!')
      .withShouldEndSession(true)
      .getResponse();
  }
};

const NavigateHomeHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest' &&
      request.intent.name === 'AMAZON.NavigateHomeIntent')
  },
  handle(handlerInput) {
    return handlerInput.responseBuilder
      .speak('Ciao a presto!')
      .withShouldEndSession(true)
      .getResponse();
  }
};

const FallbackIntentHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest' &&
      request.intent.name === 'AMAZON.FallbackIntent')
  },
  handle(handlerInput) {
    return handlerInput.responseBuilder
      .speak('Mi dispiace, ma non ho ben compreso cosa mi stai chiedendo, prova a farmi una domanda più semplice')
      .reprompt('ho dei problemi di comprensione, perdonami ma non sono così intelligente, prova a farmi una domanda più semplice')
      .withShouldEndSession(false)
      .getResponse();
  }
};

const NoIntentHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest' &&
      (request.intent.name === 'AMAZON.NoIntent'))
  },
  handle(handlerInput) {
    const intentObj = handlerInput.requestEnvelope.request.intent;
    const request = handlerInput.requestEnvelope.request;
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    sessionAttributes.previousIntent = intentObj.name;
    return handlerInput.responseBuilder
      .speak('ok, cosa vuoi fare ora?')
      .reprompt(globalRepromptText)
      .getResponse();
  }
};

const YesIntentHandler = {
  canHandle(handlerInput) {
    const request = handlerInput.requestEnvelope.request;
    return (request.type === 'IntentRequest'
      && request.intent.name === 'AMAZON.YesIntent');
  },
  handle(handlerInput) {
    const sessionAttributes = handlerInput.attributesManager.getSessionAttributes();
    var speechOutput = "";
    var reprompt = "";
    switch (sessionAttributes.previousIntent) {
      case 'FindPubInNewstand':
      case 'FindPubByAuthorAndYear':
      case 'FindPubByAuthor':
      case 'FindPubByAuthorAndPubName':
      case 'FindPubByAuthorAndPubNameAndYear':
      case 'FindPubInNewstandByTimeInterval':
      case 'FindNextPub':
      case 'FindPubByPubNameAndYear':
      case 'FindNextPub':
        if (sessionAttributes.publications === undefined && sessionAttributes.summary === undefined) {
          return handlerInput.responseBuilder
            .speak("la mia domanda non prevedeva questa risposta, prova a chiedermi qualcos'altro")
            .reprompt(globalRepromptText)
            .getResponse();
        } else if (sessionAttributes.publications.length === 0) {
          speechOutput = "Ecco la sintesi della storia " + getBreak('short') + sessionAttributes.summary + " " + getBreak('medium');
          speechOutput += " Cosa fuoi fare ora?"
          return handlerInput.responseBuilder
            .speak(speechOutput)
            .reprompt(globalRepromptText)
            .getResponse();
        } else {
          var newPubArray = new Array();
          var oldPubArray = sessionAttributes.publications;
          for (var i = 0; i < sessionAttributes.publications.length; i++) {
            if (i < maxResult) {
              speechOutput += sessionAttributes.publications[i].pubblicationName + " numero " +
                sessionAttributes.publications[i].number + getBreak('short') +
                " dal titolo " + getProsody(sessionAttributes.publications[i].pubblicationTitle) + getBreak('medium');
            } else {
              newPubArray.push({
                pubblicationName: sessionAttributes.publications[i].pubblicationName,
                number: sessionAttributes.publications[i].number,
                pubblicationTitle: sessionAttributes.publications[i].pubblicationTitle,
                pubblicationDate: sessionAttributes.publications[i].pubblicationDate,
                smallCoverURL: sessionAttributes.publications[i].smallCoverURL
              });
            }
          }
          sessionAttributes.publications = newPubArray;
          if (sessionAttributes.publications.length > 0) {
            if (sessionAttributes.previousIntent === 'FindPubInNewstand') {
              speechOutput += getBreak("medium") + " Ci sono altri " + sessionAttributes.publications.length + " fumetti in edicola, vuoi che te li dica?" + getBreak('medium');
              reprompt = "Vuoi sentire gli altri " + parseInt(sessionAttributes.publications.length - (maxResult)) + " fumetti in edicola?";
            } else if (sessionAttributes.previousIntent === 'FindPubByAuthorAndYear') {
              speechOutput += getBreak("medium") + " Ci sono altri " + sessionAttributes.publications.length + " pubblicati da " + sessionAttributes.authorName + " nel " + sessionAttributes.year + ", vuoi che te li dica?" + getBreak('medium');
              reprompt = "Vuoi sentire gli altri " + parseInt(sessionAttributes.publications.length - (maxResult)) + " fumetti pubblicati da " + sessionAttributes.authorName + " nel " + sessionAttributes.year + "?";
            } else if (sessionAttributes.previousIntent === 'FindPubByAuthor') {
              speechOutput += getBreak("medium") + " Ci sono altri " + sessionAttributes.publications.length + " pubblicati da " + sessionAttributes.authorName + ", vuoi che te li dica?" + getBreak('medium');
              reprompt = "Vuoi sentire gli altri " + parseInt(sessionAttributes.publications.length - (maxResult)) + " fumetti pubblicati da " + sessionAttributes.authorName + " ?";
            } else if (sessionAttributes.previousIntent === 'FindPubByAuthorAndPubName') {
              speechOutput += getBreak("medium") + " Ci sono altri " + sessionAttributes.publications.length + " " + sessionAttributes.pubName + " pubblicati da " + sessionAttributes.authorName + ", vuoi che te li dica?" + getBreak('medium');
              reprompt = "Vuoi sentire gli altri " + parseInt(sessionAttributes.publications.length - (maxResult)) + " " + sessionAttributes.pubName + " pubblicati da " + sessionAttributes.authorName + " ?";
            } else if (sessionAttributes.previousIntent === 'FindPubByAuthorAndPubNameAndYear') {
              speechOutput += getBreak("medium") + " Ci sono altri " + sessionAttributes.publications.length + " " + sessionAttributes.pubName + " pubblicati da " + sessionAttributes.authorName + " nel " + sessionAttributes.year + ", vuoi che te li dica?" + getBreak('medium');
              reprompt = "Vuoi sentire gli altri " + parseInt(sessionAttributes.publications.length - (maxResult)) + " fumetti pubblicati da " + sessionAttributes.authorName + " ?";
            } else if (sessionAttributes.previousIntent === 'FindPubInNewstandByTimeInterval') {
              speechOutput += getBreak("medium") + " Ci sono altri " + sessionAttributes.publications.length + " fumetti in uscita " + sessionAttributes.speakInterval + ", vuoi che te li dica?" + getBreak('medium');
              reprompt = "Vuoi sentire gli altri " + parseInt(sessionAttributes.publications.length - (maxResult)) + " fumetti in uscita " + sessionAttributes.speakInterval + " ?";
            } else if (sessionAttributes.previousIntent === 'FindNextPub') {
              speechOutput += getBreak("medium") + " Ci sono altri " + sessionAttributes.publications.length + " fumetti in uscita prossimamente, vuoi che te li dica?" + getBreak('medium');
              reprompt = "Vuoi sentire gli altri " + parseInt(sessionAttributes.publications.length - (maxResult)) + " fumetti in uscita prossimamente?";
            } else if (sessionAttributes.previousIntent === 'FindPubByPubNameAndYear') {
              speechOutput += getBreak("short") + " Ci sono altri " + parseInt(sessionAttributes.publications.length - maxResult) + " " + sessionAttributes.pubName + " usciti nel " + sessionAttributes.year + ", vuoi che te li dica?" + getBreak('medium');
              reprompt = "Vuoi sentire gli altri " + parseInt(sessionAttributes.publications.length - (maxResult)) + " " + sessionAttributes.pubName + " usciti nel " + sessionAttributes.year + "?";
            }
          } else {
            speechOutput += " Cosa vuoi fare ora?";
            reprompt += globalRepromptText;
          }

          if (supportsAPL(handlerInput)) {
            return handlerInput.responseBuilder
              .speak(speechOutput)
              .reprompt(reprompt)
              .addDirective({
                type: 'Alexa.Presentation.APL.RenderDocument',
                document: require('./apl/publication_list_template.json'),
                datasources: createDataSourceList(oldPubArray, sessionAttributes.title)
              })
              .getResponse();
          } else {
            return handlerInput.responseBuilder
              .speak(speechOutput)
              .reprompt(reprompt)
              .getResponse();
          }
        }
      case 'FindPubByPubNameAndNumber':
      case 'FindPubInNewstandByPubName':
      case 'FindPubInNewstandByAuthor':
      case 'FindPubInNewstandByPubNameAndAuthor':
      case 'FindNextPubByAuthor':
      case 'FindNextPubByPubName':
      case 'FindNextPubByPubNameAndAuthor':
      case 'FindLastPubByPubNameAuthName':
      case 'FindPubByOrdinalAndPubNameAndAuthName':
      case 'FindLastPubByPubName':
      case 'FindPubByOrdinalAndPubName':
      case 'FindLastPubByAuthName':
      case 'FindPubByOrdinalAndAuthName':
        if(sessionAttributes.summary !== undefined) { 
          speechOutput = "Ecco la sintesi della storia " + getBreak('short') + sessionAttributes.summary + " " + getBreak('medium') + " cosa vuoi fare ora?";
        } else {
          speechOutput = "Mi dispiace, mi sono dimenticata la sintesi della storia, mi potrai mai perdonare? fammi un'altra domanda!";
        }
        return handlerInput.responseBuilder
          .speak(speechOutput)
          .reprompt(globalRepromptText)
          .getResponse();
      case 'CountPubByAuthor':
        if(sessionAttributes.authorType === undefined || sessionAttributes.authorName  === undefined ||
          sessionAttributes.authorId  === undefined) {
            return handlerInput.responseBuilder
          .speak("mi dispiace, non posso procedere con la tua richiesta, mi sono dimenticata il nome dell'autore che stavi cercando "+getBreak("short")+" mi potrai mai perdonare?"+getBreak("short")+" dai fammi un'altra domanda!")
          .reprompt(globalRepromptText)
          .getResponse();
          }
        return new Promise((resolve) => {
          findPublicationByAuthor(sessionAttributes.authorType, sessionAttributes.authorId, sessionAttributes.authorName, sessionAttributes, handlerInput)
            .then(function (retObj) {
              if (supportsAPL(handlerInput) && retObj.dataSource !== undefined) {
                let template = "./apl/publication_list_template.json";
                if(!retObj.isMultiple) {
                  template = './apl/single_publication.json';
                }
                sessionAttributes.title = "Fumetti di " + sessionAttributes.authorName;
                resolve(handlerInput.responseBuilder
                  .speak(retObj.speechOutput)
                  .reprompt(retObj.reprompt)
                  .addDirective({
                    type: 'Alexa.Presentation.APL.RenderDocument',
                    document: require(template),
                    datasources: retObj.dataSource
                  })
                  .getResponse());
              } else {
                resolve(handlerInput.responseBuilder
                  .speak(retObj.speechOutput)
                  .reprompt(retObj.reprompt)
                  .getResponse());
              }
            },
              function (error) {
                console.log("Si è verificato l'errore " + error.message);
              });
        });
      case 'CountPubByAuthorAndYear':
        if(sessionAttributes.authorType === undefined || sessionAttributes.authorName  === undefined ||
          sessionAttributes.authorId  === undefined || sessionAttributes.year  === undefined) {
            return handlerInput.responseBuilder
          .speak("mi dispiace, non posso procedere con la tua richiesta, mi sono dimenticata il nome dell'autore che stavi cercando "+getBreak("short")+" mi potrai mai perdonare?"+getBreak("short")+" dai fammi un'altra domanda!")
          .reprompt(globalRepromptText)
          .getResponse();
          }
        return new Promise((resolve) => {
          findPublicationByAuthorAndYear(sessionAttributes.authorType, sessionAttributes.authorName, sessionAttributes.authorId, sessionAttributes.year, sessionAttributes, handlerInput)
            .then(function (retObj) {
              if (supportsAPL(handlerInput) && retObj.dataSource !== undefined) {
                let template = "./apl/publication_list_template.json";
                if(!retObj.isMultiple) {
                  template = './apl/single_publication.json';
                }
                sessionAttributes.title = "Fumetti di " + sessionAttributes.authorName + " del " + sessionAttributes.year;
                resolve(handlerInput.responseBuilder
                  .speak(retObj.speechOutput)
                  .reprompt(retObj.reprompt)
                  .addDirective({
                    type: 'Alexa.Presentation.APL.RenderDocument',
                    document: require(template),
                    datasources: retObj.dataSource
                  })
                  .getResponse());
              } else {
                resolve(handlerInput.responseBuilder
                  .speak(retObj.speechOutput)
                  .reprompt(retObj.reprompt)
                  .getResponse());
              }
            },
              function (error) {
                console.log("Si è verificato l'errore " + error.message);
              });
        });
      case 'CountPubByPubNameAndAuthor':
        if(sessionAttributes.authorType === undefined || sessionAttributes.authorName  === undefined ||
          sessionAttributes.authorId  === undefined || sessionAttributes.pubName  === undefined) {
            return handlerInput.responseBuilder
          .speak("mi dispiace, non posso procedere con la tua richiesta, mi sono dimenticata il nome dell'autore che stavi cercando "+getBreak("short")+" mi potrai mai perdonare?"+getBreak("short")+" dai fammi un'altra domanda!")
          .reprompt(globalRepromptText)
          .getResponse();
          }
        return new Promise((resolve) => {
          findPublicationByAuthorAndPubName(sessionAttributes.authorType, sessionAttributes.authorId, sessionAttributes.authorName, sessionAttributes.pubName, sessionAttributes, handlerInput)
            .then(function (retObj) {
              if (supportsAPL(handlerInput) && retObj.dataSource !== undefined) {
                let template = './apl/publication_list_template.json';
                if(!retObj.isMultiple) {
                  template = "./apl/single_publication.json";
                }
                sessionAttributes.title = sessionAttributes.pubName + " di " + sessionAttributes.authorName;
                resolve(handlerInput.responseBuilder
                  .speak(retObj.speechOutput)
                  .reprompt(retObj.reprompt)
                  .addDirective({
                    type: 'Alexa.Presentation.APL.RenderDocument',
                    document: require(template),
                    datasources: retObj.dataSource
                  })
                  .getResponse());
              } else {
                resolve(handlerInput.responseBuilder
                  .speak(retObj.speechOutput)
                  .reprompt(retObj.reprompt)
                  .getResponse());
              }
            },
              function (error) {
                console.log("Si è verificato l'errore " + error.message);
              });
        });
      case 'CountPubByPubNameAndAuthorAndYear':
        if(sessionAttributes.authorType === undefined || sessionAttributes.authorName  === undefined ||
          sessionAttributes.authorId  === undefined || sessionAttributes.pubName  === undefined ||
          sessionAttributes.year  === undefined) {
            return handlerInput.responseBuilder
          .speak("mi dispiace, non posso procedere con la tua richiesta, mi sono dimenticata il nome dell'autore che stavi cercando "+getBreak("short")+" mi potrai mai perdonare?"+getBreak("short")+" dai fammi un'altra domanda!")
          .reprompt(globalRepromptText)
          .getResponse();
          }
        return new Promise((resolve) => {
          findPublicationByAuthorAndPubNameAndYear(sessionAttributes.authorType, sessionAttributes.authorId, sessionAttributes.authorName, sessionAttributes.pubName, sessionAttributes.year, sessionAttributes, handlerInput)
            .then(function (retObj) {
              if (supportsAPL(handlerInput) && retObj.dataSource !== undefined) {
                sessionAttributes.title = "Fumetti di " + sessionAttributes.pubName + " pubblicati da " + sessionAttributes.authorName + " nel " + sessionAttributes.year;
                let template = './apl/publication_list_template.json';
                if(!retObj.isMultiple) {
                  template = './apl/single_publication.json';
                }
                resolve(handlerInput.responseBuilder
                  .speak(retObj.speechOutput)
                  .reprompt(retObj.reprompt)
                  .addDirective({
                    type: 'Alexa.Presentation.APL.RenderDocument',
                    document: require(template),
                    datasources: retObj.dataSource
                  })
                  .getResponse());
              } else {
                resolve(handlerInput.responseBuilder
                  .speak(retObj.speechOutput)
                  .reprompt(retObj.reprompt)
                  .getResponse());
              }
            },
              function (error) {
                console.log("Si è verificato l'errore " + error.message);
              });
        });
      default:
        speechOutput = "la mia domanda non prevedeva questa risposta, prova a chiedermi qualcos'altro";
        return handlerInput.responseBuilder
          .speak(speechOutput)
          .reprompt(globalRepromptText)
          .getResponse();
    }
  }
};

const SKILL_NAME = 'Catalogo fumetti';

function keyGenerator(requestEnvelope) {
  /*
    if (requestEnvelope
        && requestEnvelope.context
        && requestEnvelope.context.System
        && requestEnvelope.context.System.application
        && requestEnvelope.context.System.application.applicationId) {
      return requestEnvelope.context.System.application.applicationId; 
    }
  */
  if (requestEnvelope
    && requestEnvelope.context
    && requestEnvelope.context.System
    && requestEnvelope.context.System.user
    && requestEnvelope.context.System.user.userId) {
    return requestEnvelope.context.System.user.userId;
  }
  throw 'Cannot retrieve app id from request envelope!';
}

exports.handler = skillBuilder
  .withPersistenceAdapter(
    new DynamoDbPersistenceAdapter({
      tableName: 'settings',
      createTable: true,
      partitionKeyGenerator: keyGenerator
    })
  )
  .addRequestHandlers(
    LaunchRequestHandler,
    StopIntentHandler,
    YesIntentHandler,
    NoIntentHandler,
    HelpIntentHandler,
    FallbackIntentHandler,
    GetExampleRequestHandler,
    NavigateHomeHandler,
    CountPubByAuthor,
    CountPubByAuthorAndYear,
    CountPubByPubNameAndAuthor,
    CountPubByPubNameAndAuthorAndYear,
    FindPubByPubNameAndYear, //FindPubAttrByPubNameAndYear,
    FindPubByPubNameAndNumber, //FindPubAttrByPubNameAndNumber,
    FindPubByAuthor, //FindPubAttrByAuthor,
    FindPubByAuthorAndYear, //FindPubAttrByAuthorAndYear,
    FindPubByAuthorAndPubNameAndYear,
    FindPubByAuthorAndPubName,
    FindPubInNewstand,//FindPubAttrInNewstand,
    FindPubInNewstandByTimeInterval,
    FindPubInNewstandByPubName,//FindPubAttrInNewstandByPubName,
    FindPubInNewstandByAuthor,
    FindPubInNewstandByPubNameAndAuthor,
    FindNextPub, //FindNextPubAttr
    FindNextPubByAuthor,
    FindNextPubByPubNameAndAuthor,
    FindNextPubByPubName, //FindNextPubAttrByPubName,    
    FindPubByOrdinalAndPubName,
    FindPubByOrdinalAndPubNameAndAuthName,
    FindPubByOrdinalAndAuthName
    /*,AdditionalInfo*/
  )
  .addErrorHandlers(ErrorHandler)
  .lambda();

